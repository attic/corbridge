m4_define([corbridge_major], [0])
m4_define([corbridge_minor], [2])
m4_define([corbridge_micro], [3])

m4_define([corbridge_version], [corbridge_major.corbridge_minor.corbridge_micro])

# Before making a release, the CORBRIDGE_LT_VERSION string should be modified. The
# string is of the form c:r:a. Follow these instructions sequentially:
#   1. If the library source code has changed at all since the last update, then
#      increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#   2. If any interfaces have been added, removed, or changed since the last
#      update, increment current, and set revision to 0.
#   3. If any interfaces have been added since the last public release, then
#      increment age.
#   4. If any interfaces have been removed or changed since the last public
#      release, then set age to 0.
m4_define([corbridge_lt_version], [0:2:0])

AC_INIT([corbridge], corbridge_version)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_MAINTAINER_MODE([enable])

AC_SUBST([CORBRIDGE_LT_VERSION],corbridge_lt_version)
AC_SUBST(CORBRIDGE_VERSION,corbridge_version)

# Incremented if the API has incompatible changes
AC_SUBST([CORBRIDGE_API_VERSION],corbridge_major)

AM_INIT_AUTOMAKE([-Wno-portability tar-ustar])
AM_SILENT_RULES([yes])

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_STDC_HEADERS
AC_SEARCH_LIBS([strerror],[cposix])
AC_CHECK_FUNCS([memset])
AC_CHECK_HEADERS([fcntl.h stdlib.h string.h])

LT_INIT

AC_ARG_VAR(GDBUS_CODEGEN, [the gdbus-codegen programme ])
AC_PATH_PROG(GDBUS_CODEGEN, gdbus-codegen)
if test -z "$GDBUS_CODEGEN" ; then
  AC_MSG_ERROR([gdbus-codegen not found])
fi

#Check if Gobject-introspection enabled
AC_ARG_ENABLE([introspection],
AS_HELP_STRING([--enable-introspection], [build with gobject introspection [default=yes]]),[],[enable_introspection=yes])
AM_CONDITIONAL(HAVE_INTROSPECTION, test "x$enable_introspection" = "xyes")

# check for gobject-introspection
GOBJECT_INTROSPECTION_CHECK([1.31.1])

dnl check for hotdoc
HOTDOC_CHECK([0.8], [c, dbus])

CORBRIDGE_PACKAGES_PUBLIC_GLIB="glib-2.0"
CORBRIDGE_PACKAGES_PRIVATE_GLIB="gio-2.0 gthread-2.0 gio-unix-2.0"

CORBRIDGE_PACKAGES_PUBLIC_DBUS=""
CORBRIDGE_PACKAGES_PRIVATE_DBUS="dbus-glib-1 >= 0.98 dbus-1"

CORBRIDGE_PACKAGES_PUBLIC_SYSTEMD=""
CORBRIDGE_PACKAGES_PRIVATE_SYSTEMD="libsystemd"

CORBRIDGE_PACKAGES_PUBLIC="$CORBRIDGE_PACKAGES_PUBLIC_GLIB $CORBRIDGE_PACKAGES_PUBLIC_DBUS $CORBRIDGE_PACKAGES_PUBLIC_SYSTEMD"
CORBRIDGE_PACKAGES_PRIVATE="$CORBRIDGE_PACKAGES_PRIVATE_GLIB $CORBRIDGE_PACKAGES_PRIVATE_DBUS $CORBRIDGE_PACKAGES_PRIVATE_SYSTEMD"

AC_SUBST([CORBRIDGE_PACKAGES_PUBLIC])
AC_SUBST([CORBRIDGE_PACKAGES_PRIVATE])

PKG_CHECK_MODULES([GLIB], [$CORBRIDGE_PACKAGES_PUBLIC_DBUS $CORBRIDGE_PACKAGES_PRIVATE_GLIB])
PKG_CHECK_MODULES([DBUS], [$CORBRIDGE_PACKAGES_PUBLIC_DBUS $CORBRIDGE_PACKAGES_PRIVATE_DBUS])
PKG_CHECK_MODULES([SYSTEMD], [$CORBRIDGE_PACKAGES_PUBLIC_SYSTEMD $CORBRIDGE_PACKAGES_PRIVATE_SYSTEMD])
#PKG_CHECK_MODULES([AUTOCONF], [$CORBRIDGE_PACKAGES_PUBLIC_AUTOCONF $CORBRIDGE_PACKAGES_PRIVATE_AUTOCONF])
#AX_PKG_CHECK_MODULES([GLIB], [glib-2.0 gio-2.0],[gthread-2.0 gio-unix-2.0])
#AX_PKG_CHECK_MODULES([DBUS], [],[dbus-glib-1 >= 0.98 dbus-1])
#AX_PKG_CHECK_MODULES([SYSTEMD], [],[libsystemd-daemon])

AX_IS_RELEASE([micro-version])

dnl TODO: fix the warnings, then just "AX_COMPILER_FLAGS" is enough
AX_COMPILER_FLAGS([], [], [yes])

CORBRIDGE_FI='-lcorbridgecontrol -lcorbridgeaddressbook'
AC_SUBST(CORBRIDGE_FI)

AC_ARG_WITH([systemduserunitdir],
  AC_HELP_STRING([--with-systemduserunitdir=DIR],
    [path to systemd user service directory]),
  [systemduserunitdir="$withval"],
  [systemduserunitdir='${prefix}/lib/systemd/user'])
AC_SUBST([systemduserunitdir])

#installed tests
AC_ARG_ENABLE(modular_tests,
         AS_HELP_STRING([--disable-modular-tests],
         [Disable build of test programs (default: no)]),,
         [enable_modular_tests=yes])

AC_ARG_ENABLE(installed_tests,
         AS_HELP_STRING([--enable-installed-tests],
         [Install test programs (default: no)]),,
         [enable_installed_tests=no])

AM_CONDITIONAL(BUILD_MODULAR_TESTS,
         [test "$enable_modular_tests" = "yes" ||
         test "$enable_installed_tests" = "yes"])
AM_CONDITIONAL(BUILDOPT_INSTALL_TESTS, test "$enable_installed_tests" = "yes")


# Code coverage
AX_CODE_COVERAGE

# Output files
AC_CONFIG_FILES([
Makefile
corbridge.pc

docs/Makefile
docs/reference/Makefile
fi/Makefile
src/org_bluez/Makefile
src/Makefile
tests/Makefile
])

AC_OUTPUT


dnl Summary
echo "                      corbridge "
echo "                      --------------"
echo "  documentation                  : ${enable_documentation}
        code coverage                  : ${enable_code_coverage}
        compiler warings               : ${enable_compile_warnings}
        Test suite                     : ${enable_modular_tests}
        Introspection                  : ${found_introspection}
"
