/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _BLUEZ_MNG_H
#define _BLUEZ_MNG_H

#include <glib-object.h>

#define PBAPPROFILE "Phonebook_Access"
#define SYNCMLPROFILE "SyncMLClient"

#include "corbridge.h"

G_BEGIN_DECLS

#define BLUEZ_TYPE_MNG bluez_mng_get_type()

#define BLUEZ_MNG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  BLUEZ_TYPE_MNG, BluezMng))

#define BLUEZ_MNG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  BLUEZ_TYPE_MNG, BluezMngClass))

#define BLUEZ_IS_MNG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  BLUEZ_TYPE_MNG))

#define BLUEZ_IS_MNG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  BLUEZ_TYPE_MNG))

#define BLUEZ_MNG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  BLUEZ_TYPE_MNG, BluezMngClass))

typedef struct _BluezMng BluezMng;
typedef struct _BluezMngClass BluezMngClass;
typedef struct _BluezMngPrivate BluezMngPrivate;

struct _BluezMng
{
  GObject parent;

  BluezMngPrivate *priv;
};

struct _BluezMngClass
{
  GObjectClass parent_class;
};

GType bluez_mng_get_type (void) G_GNUC_CONST;

BluezMng *bluez_mng_new (void);
BluezMng *bluez_mgr_get_object(void);
CorbridgeControl *get_corbridge_control_object(BluezMng *obj);
CorbridgeAddressBook *get_corbridge_address_book_object(BluezMng *self);

G_END_DECLS

#endif /* _BLUEZ_MNG_H */
