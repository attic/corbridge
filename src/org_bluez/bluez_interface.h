/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef BLUEZ_INTERFACE_H_
#define BLUEZ_INTERFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>
#include <ctype.h>
#include <string.h>

typedef struct _BtInfo
{
  gchar *address;
  gchar *capability;
} BtInfo;


void initialise_org_bluez_gdbus_proxy();
void start_bt_scan(const gchar *arg_app_name);
void stop_bt_scan(const gchar *arg_app_name);
void create_bt_paired_device(const gchar *arg_address, const gchar *arg_capability, const gchar *arg_pincode);
void start_org_bluez_agent_service();
void replace_string(gchar *orig_str, gchar *mod_str);
void create_proxy_for_adapter_property_change();
void get_device_properties_and_emit_signal(const gchar *object_path);
void parse_device_properties(GVariant * device_properties);
void bt_unpair_device(const gchar *arg_address);
void get_paired_devices();
void trigger_a2dp_connect(const gchar *address);
void trigger_a2dp_disconnect(const gchar *address);
void a2dp_get_properties(const gchar *address);
void set_property_to_onoff_bluetooth(const gboolean value);

typedef enum {

	BLUEZ_SRV_DEBUG          = 1 << 0,

}BluezSrvDebugFlag;
#define BLUEZ_SRV_HAS_DEBUG               ((bluez_service_debug_flags) & 1)
#define BLUEZ_DNLD_PRINT( a ...) \
    if (G_LIKELY (BLUEZ_SRV_HAS_DEBUG )) \
    {			            	\
        g_print("BLUEZService::> " a);		\
    }
extern guint bluez_service_debug_flags;

#endif /* BLUEZ_INTERFACE_H_ */
