/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "bluez_interface.h"
#include <bluez_manager_gdbus_fi.h>
#include <bluez_adapter_gdbus_fi.h>
#include <bluez_agent_gdbus_fi.h>
#include <bluez_service_gdbus_fi.h>
#include "bluez-mng.h"

#define BLUEZ_INTERFACE_DEBUG 0
#if BLUEZ_INTERFACE_DEBUG
  #define BLUEZ_INTERFACE_PRINT(...)       g_print(__VA_ARGS__)
#else
  #define BLUEZ_INTERFACE_PRINT(...)
#endif

guint  bluez_service_debug_flags = 0;

static const GDebugKey bluez_debug_keys[] = {
  { "bluez-mgr",          BLUEZ_SRV_DEBUG }
};

OrgBluezManager * org_bluez_manager;
OrgBluezAdapter * global_adapter_proxy;
OrgBluezAdapter *device_found_proxy;
OrgBluezAgent *org_bluez_agent_proxy;
gulong device_found_handler_id;
gulong device_not_found_handler_id;
gchar *global_adapter_path;
GHashTable *ghash_table_pincode;
gchar *agent_path;

void custom_ghashtable_free(gpointer pvalue)
{
	if (pvalue) {
		g_free(pvalue);
		pvalue = NULL;
	}
}

void g_hash_for_pincode_init()
{
	ghash_table_pincode = g_hash_table_new_full(g_str_hash,
												   g_str_equal,
												   custom_ghashtable_free,
												   custom_ghashtable_free
			 	 	 	 	 	 	 	 	 	 	 );
}

static const char *corbridge_bt_convert_uuid16_to_string (guint u16_uuid, const char *uuid)
{
  /* Short names from https://www.bluetooth.org/Technical/AssignedNumbers/service_discovery.htm */
  const char *ret_string = NULL;
  switch (u16_uuid)
  {
    case 0x1101:
      ret_string = "SerialPort";
      break;
    case 0x1103:
      ret_string = "DialupNetworking";
      break;
    case 0x1104:
      ret_string = "IrMCSync";
      break;
    case 0x1105:
      ret_string = "OBEXObjectPush";
      break;
    case 0x1106:
      ret_string = "OBEXFileTransfer";
      break;
    case 0x1108:
      ret_string = "HSP";
      break;
    case 0x110A:
      ret_string = "AudioSource";
      break;
    case 0x110B:
      ret_string = "AudioSink";
      break;
    case 0x110c:
      ret_string = "A/V_RemoteControlTarget";
      break;
    case 0x110e:
      ret_string = "A/V_RemoteControl";
      break;
    case 0x1112:
      ret_string = "Headset_-_AG";
      break;
    case 0x1115:
      ret_string = "PANU";
      break;
    case 0x1116:
      ret_string = "NAP";
      break;
    case 0x1117:
      ret_string = "GN";
      break;
    case 0x111b:
      ret_string = "ImagingResponder";
      break;
    case 0x111e:
      ret_string = "Handsfree";
      break;
    case 0x111F:
      ret_string = "HandsfreeAudioGateway";
      break;
    case 0x1124:
      ret_string = "HumanInterfaceDeviceService";
      break;
    case 0x112d:
      ret_string = "SIM_Access";
      break;
    case 0x112E:
      ret_string = "Phonebook_Access";
      break;
    case 0x112F:
      ret_string = "Phonebook_Access";
      break;
    case 0x1130:
      ret_string = "Phonebook_Access";
      break;
    case 0x1132:
      ret_string = "MAP";
      break;
    case 0x1133:
      ret_string = "MAP";
      break;
    case 0x1134:
      ret_string = "MAP";
      break;
    case 0x1201:
      ret_string = "GenericNetworking";
      break;
    case 0x1203:
      ret_string = "GenericAudio";
      break;
    case 0x1303:
      ret_string = "VideoSource";
      break;
    case 0x8e771301:
    case 0x8e771303:
      ret_string = "SEMC HLA";
      break;
    case 0x8e771401:
      ret_string = "SEMC Watch Phone";
      break;
    default:
      g_warning ("Unhandled UUID %s (0x%x)", uuid, u16_uuid);
      ret_string = NULL;
      break;
  }
  return ret_string;
}

static const char *corbridge_bt_convert_uuid16_custom_to_string (guint u16_uuid, const char *uuid_string)
{
  const char *ret_string = NULL;
  switch (u16_uuid)
  {
    case 0x2:
      ret_string = "SyncMLClient";
      break;
    case 0x4:
      ret_string = "SyncML DM Client";
      break;
    case 0x5005:
      ret_string = "Nokia OBEX PC Suite Services";
      break;
    case 0x5601:
      ret_string = "Nokia SyncML Server";
      break;
    default:
      g_warning ("Unhandled custom UUID %s (0x%x)", uuid_string, u16_uuid);
      ret_string = NULL;
      break;
  }
  return ret_string;
}


static const char *corbridge_bt_convert_uuid_to_string (const char *uuid_string)
{
  guint u16_uuid;
  char **uuid_parts;
  gboolean isCustom = FALSE;

  if ((TRUE == g_str_has_suffix (uuid_string, "-0000-1000-8000-0002ee000002")) ||
      (TRUE == g_str_has_suffix (uuid_string, "-0000-1000-8000-0002ee000001")))
  {
    isCustom = TRUE;
  }

  uuid_parts = g_strsplit (uuid_string, "-", -1);
  if ((uuid_parts == NULL) || (uuid_parts[0] == NULL))
  {
    g_strfreev (uuid_parts);
    return NULL;
  }

  u16_uuid = g_ascii_strtoull (uuid_parts[0], NULL, 16);
  g_strfreev (uuid_parts);

  if (u16_uuid == 0)
  {
    return NULL;
  }

  if (isCustom == FALSE)
  {
    return corbridge_bt_convert_uuid16_to_string (u16_uuid, uuid_string);
  }
  else
  {
    return corbridge_bt_convert_uuid16_custom_to_string (u16_uuid, uuid_string);
  }
}

void org_bluez_adapter_property_changed (OrgBluezAdapter *object, const gchar *arg_name, GVariant *arg_value)
{

	BLUEZ_DNLD_PRINT("\n\n\n");
	BLUEZ_DNLD_PRINT(" Start \n");
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT(" Changed property name is %s\n", arg_name);
	BLUEZ_DNLD_PRINT(" Changed property value is %s\n", g_variant_print(arg_value, 1));
	BLUEZ_DNLD_PRINT(" Stop \n");
	BLUEZ_DNLD_PRINT("\n\n\n");
}

void org_bluez_adapter_device_removed(OrgBluezAdapter *object, const gchar *arg_device)
{
  BLUEZ_DNLD_PRINT("\n\n\n");
  BLUEZ_DNLD_PRINT(" Start \n");
  BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
  BLUEZ_DNLD_PRINT(" Device removed is %s\n", arg_device);
  BluezMng *self = bluez_mgr_get_object();
  CorbridgeControl *obj = get_corbridge_control_object(self);
  if(obj != NULL)
  corbridge_control_emit_connection_error(obj, arg_device, "Removed Successfully");
  BLUEZ_DNLD_PRINT(" Stop \n");
  BLUEZ_DNLD_PRINT("\n\n\n");
}

static void org_bluez_device_found (OrgBluezAdapter *object,
							 const gchar *arg_address,
							 GVariant *arg_values)
{
	BLUEZ_DNLD_PRINT("\n\n");
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GVariantIter *bluetooth_info ;
	gchar *key;
	GVariant *value;
	g_variant_get(arg_values, "a{sv}",&bluetooth_info);

	while (g_variant_iter_next(bluetooth_info, "{sv}", &key, &value))
	{
		BLUEZ_DNLD_PRINT("bluetooth --> %s:%s\n", key, g_variant_print(value, 1));
	}

	BLUEZ_DNLD_PRINT("\n\n");
	GVariant *return_properties = g_variant_new("(@a{sv})",arg_values);

	BluezMng *self = bluez_mgr_get_object();
	CorbridgeControl *obj = get_corbridge_control_object(self);
	if(obj != NULL)
	corbridge_control_emit_bt_device_found(obj, return_properties);
}

static void org_bluez_device_disappeared(OrgBluezAdapter *object, const gchar *arg_address)
{
	BLUEZ_DNLD_PRINT("\n\n");
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT(" Device disappeared address %s\n", arg_address);
	BLUEZ_DNLD_PRINT("\n\n");

	BluezMng *self = bluez_mgr_get_object();
	CorbridgeControl *obj = get_corbridge_control_object(self);
	if(obj != NULL)
	corbridge_control_emit_bt_device_disappeared(obj, arg_address);

}


static void disconnect_to_device_found_signal()
{
	BLUEZ_DNLD_PRINT("\n\n");
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	if(device_found_proxy)
	{
		g_signal_handler_disconnect(device_found_proxy, device_found_handler_id);
		g_signal_handler_disconnect(device_found_proxy, device_not_found_handler_id);
		g_object_unref(device_found_proxy);
		device_found_proxy = NULL;
		BLUEZ_DNLD_PRINT("Disconnect to signal successful\n");
	}
	BLUEZ_DNLD_PRINT("\n\n");
}

static void org_bluez_adapter_path_device_found_proxy_created_clb( GObject *source_object,
                                			 	 GAsyncResult *res,
                                			 	 gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	device_found_proxy = org_bluez_adapter_proxy_new_finish (res, &error);

	 if( (device_found_proxy == NULL) || error )
	 {
		  g_warning ("Error initializing the org.bluez adapter proxy %s", error ? error->message : "no error given.");
		  g_error_free(error);
		  error = NULL;
	 }
	 else
	 {
		BLUEZ_DNLD_PRINT(" device-found signal registering....\n");
		device_found_handler_id = g_signal_connect(device_found_proxy, "device-found", G_CALLBACK(org_bluez_device_found), NULL );
		device_not_found_handler_id = g_signal_connect(device_found_proxy, "device-disappeared", G_CALLBACK(org_bluez_device_disappeared), NULL );
	 }
}


static void org_bluez_device_found_name_appeared (GDBusConnection *connection,
                            const gchar    *name,
                            const gchar    *name_owner,
                            gpointer        user_data)

{
	 BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	 if(global_adapter_path)
	 {
		 BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
		 org_bluez_adapter_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", global_adapter_path, NULL, org_bluez_adapter_path_device_found_proxy_created_clb, user_data);
	 }

}



static void org_bluez_device_found_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" org.bluez Adapter Name_vanished \n");
}

static void connect_to_device_found_signal()
{

	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                            "org.bluez",
                            G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                            org_bluez_device_found_name_appeared,
                            org_bluez_device_found_name_vanished,
                            NULL,
                            NULL);

}

void org_bluez_manager_property_changed (OrgBluezManager *object,
								 const gchar *arg_name,
								 GVariant *arg_value)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT(" Changed property name is %s\n", arg_name);
	BLUEZ_DNLD_PRINT(" Changed property value is %s\n\n\n", g_variant_print(arg_value, 1));

}


void org_bluez_adapter_added (OrgBluezManager *object,
							 const gchar *arg_adapter)
{
	BLUEZ_DNLD_PRINT(" Function %s\n", __FUNCTION__);
	BLUEZ_DNLD_PRINT(" Adapter added is %s\n", arg_adapter);
	BluezMng *self = bluez_mgr_get_object();
	CorbridgeControl *obj = get_corbridge_control_object(self);
	if(obj != NULL)
	corbridge_control_emit_connection_error(obj, arg_adapter, "BT Adapter added");

	GError *error_default_adapter = NULL;
	gchar *default_adapter_path = NULL;
	org_bluez_manager_call_default_adapter_sync(org_bluez_manager, &default_adapter_path, NULL, &error_default_adapter);
	if (error_default_adapter)
	{
		g_warning("Error in getting default adapter path %s", error_default_adapter ? error_default_adapter->message : "no error given.");
		g_error_free(error_default_adapter);
		error_default_adapter = NULL;
	}
	else
	{
		g_free(global_adapter_path);
		global_adapter_path = g_strdup(default_adapter_path);
		BLUEZ_DNLD_PRINT(" default Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
		create_proxy_for_adapter_property_change();
	}
}

void org_bluez_adapter_removed(OrgBluezManager *object, const gchar *arg_adapter)
{
		BLUEZ_DNLD_PRINT(" Function %s\n", __FUNCTION__);
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if(obj != NULL)
		corbridge_control_emit_connection_error(obj, arg_adapter, "BT Adapter removed");

	    GError *error_default_adapter = NULL;
		gchar *default_adapter_path = NULL;
		org_bluez_manager_call_default_adapter_sync(org_bluez_manager, &default_adapter_path, NULL, &error_default_adapter);
		if (error_default_adapter)
		{
			g_warning("Error in getting default adapter path %s", error_default_adapter ? error_default_adapter->message : "no error given.");
			g_error_free(error_default_adapter);
			error_default_adapter = NULL;
			g_free(global_adapter_path);
			global_adapter_path = NULL;
		}
		else
		{
			BLUEZ_DNLD_PRINT(" default Adapter path is %s\n", default_adapter_path);

			g_free(global_adapter_path);
			global_adapter_path = g_strdup(default_adapter_path);
			create_proxy_for_adapter_property_change();
		}

}

static void org_bluez_adapter_path_start_discovery_proxy_created_clb( GObject *source_object,
                                			 	 GAsyncResult *res,
                                			 	 gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	GError *error_start_discovery = NULL;
	OrgBluezAdapter * org_bluez_adapter = NULL;
	org_bluez_adapter = org_bluez_adapter_proxy_new_finish (res, &error);

	 if( (org_bluez_adapter == NULL) || error )
	 {
		g_warning("Error initializing the org.bluez adapter proxy %s", error ? error->message : "no error given.");
		gchar *striped_error = NULL;
		gchar *dup_error = g_strdup(error->message);
		striped_error = g_strrstr(dup_error, ":");
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if (striped_error == NULL )
		{
			corbridge_control_emit_connection_error(obj, "Scan is initiating ....", error->message);
		}
		else
		{
			corbridge_control_emit_connection_error(obj, "Scan is initiating ....", striped_error + 1);
		}

		g_error_free(error);
		error = NULL;
		g_free(dup_error);
		dup_error = NULL;
	}
	 else
	 {
		org_bluez_adapter_call_start_discovery_sync(org_bluez_adapter, NULL, &error_start_discovery);
		if (error_start_discovery)
		{
			g_warning("Error initializing the org.bluez adapter proxy %s", error_start_discovery ? error_start_discovery->message : "no error given.");
			gchar *striped_error = NULL;
			gchar *dup_error = g_strdup(error_start_discovery->message);
			striped_error = g_strrstr(dup_error, ":");

			BluezMng *self = bluez_mgr_get_object();
			CorbridgeControl *obj = get_corbridge_control_object(self);

			if (striped_error == NULL )
			{
				corbridge_control_emit_connection_error(obj, "Scan is initiating ....", error_start_discovery->message);

			}
			else
			{
				corbridge_control_emit_connection_error(obj, "Scan is initiating ....", striped_error + 1);
			}

			g_free(dup_error);
			dup_error = NULL;
			g_error_free(error_start_discovery);
			error_start_discovery = NULL;
		}
		else
		{
			BLUEZ_DNLD_PRINT(" start discovery is successful\n");
		}
	}

	 if(org_bluez_adapter)
		 g_object_unref(org_bluez_adapter);
}





static void org_bluez_adapter_path_stop_discovery_proxy_created_clb( GObject *source_object,
                                			 	 GAsyncResult *res,
                                			 	 gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	GError *error_stop_discovery = NULL;
	OrgBluezAdapter * org_bluez_adapter = NULL;
	org_bluez_adapter = org_bluez_adapter_proxy_new_finish (res, &error);

	 if( (org_bluez_adapter == NULL) || error )
	 {
		g_warning("Error initializing the org.bluez adapter proxy %s", error ? error->message : "no error given.");
		gchar *striped_error = NULL;
		gchar *dup_error = g_strdup(error->message);
		striped_error = g_strrstr(dup_error, ":");

		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);

		if (striped_error == NULL )
		{
			corbridge_control_emit_connection_error(obj, "Scan stop is initiating ....", error->message);
		}
		else
		{
			corbridge_control_emit_connection_error(obj, "Scan stop is initiating ....", striped_error + 1);
		}

		g_error_free(error);
		error = NULL;
		g_free(dup_error);
		dup_error = NULL;
	}
	 else
	 {
		org_bluez_adapter_call_stop_discovery_sync(org_bluez_adapter, NULL, &error_stop_discovery);
		if (error_stop_discovery)
		{
			g_warning("Error in calling stop discovery function %s", error_stop_discovery ? error_stop_discovery->message : "no error given.");

			gchar *striped_error = NULL;
			gchar *dup_error = g_strdup(error_stop_discovery->message);
			striped_error = g_strrstr(dup_error, ":");

			BluezMng *self = bluez_mgr_get_object();
			CorbridgeControl *obj = get_corbridge_control_object(self);

			if (striped_error == NULL )
			{
				corbridge_control_emit_connection_error(obj, "Scan stop is initiating ....", error_stop_discovery->message);

			}
			else
			{
				if(g_strcmp0(" Invalid discovery session", striped_error + 1) == 0)
				{
					corbridge_control_emit_connection_error(obj, "Scan stop is initiating ....", "Scan stop is already done");
				}
				else
				{
					corbridge_control_emit_connection_error(obj, "Scan stop is initiating ....", striped_error + 1);
				}
			}

			g_free(dup_error);
			dup_error = NULL;
			g_error_free(error_stop_discovery);
			error_stop_discovery = NULL;
		}
		else
		{
			BLUEZ_DNLD_PRINT(" stop discovery is successful\n");
		}
	 }

	 if(org_bluez_adapter)
		 g_object_unref(org_bluez_adapter);
}




static void org_bluez_manager_proxy_created_clb( GObject *source_object,
                                			 	 GAsyncResult *res,
                                			 	 gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	 GError *error = NULL;
	  /* finishes the proxy creation and gets the proxy ptr */
	 org_bluez_manager = org_bluez_manager_proxy_new_finish (res , &error);

	 if( (org_bluez_manager == NULL) || error )
	 {
		  g_warning ("Error initializing the org.bluez mng proxy %s", error ? error->message : "no error given.");
		  g_error_free(error);
		  error = NULL;
	 }
	 else
	 {
		 g_signal_connect(org_bluez_manager, "property-changed", G_CALLBACK(org_bluez_manager_property_changed), NULL);
		 g_signal_connect(org_bluez_manager, "adapter-added", G_CALLBACK(org_bluez_adapter_added), NULL);
		 g_signal_connect(org_bluez_manager, "adapter-removed", G_CALLBACK(org_bluez_adapter_removed), NULL);
		 //new_register_for_bluez_agent("DisplayYesNo");
		 start_org_bluez_agent_service();
		 g_hash_for_pincode_init();

		 BluezMng *self = bluez_mgr_get_object();
		 CorbridgeControl *obj = get_corbridge_control_object(self);
		 GError *error_default_adapter = NULL;
		 gchar *default_adapter_path = NULL;
		 org_bluez_manager_call_default_adapter_sync(org_bluez_manager, &default_adapter_path, NULL, &error_default_adapter);
		 if (error_default_adapter)
		 {
			g_warning("Error in getting default adapter path %s", error_default_adapter ? error_default_adapter->message : "no error given.");
			g_error_free(error_default_adapter);
			error_default_adapter = NULL;
		 }
		else
		 {
			g_free(global_adapter_path);
			global_adapter_path = g_strdup(default_adapter_path);
			BLUEZ_DNLD_PRINT(" Default Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
			create_proxy_for_adapter_property_change();
		 }

	 }
}




static void org_bluez_mng_name_appeared (GDBusConnection *connection,
                            const gchar    *name,
                            const gchar    *name_owner,
                            gpointer        user_data)

{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
    org_bluez_manager_proxy_new(connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", "/", NULL,  org_bluez_manager_proxy_created_clb, connection);
}

static void org_bluez_mng_name_vanished(GDBusConnection *connection,const gchar *name,gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	if (NULL != org_bluez_manager) {
		g_object_unref(org_bluez_manager);
	}
	g_hash_table_destroy(ghash_table_pincode);
	}

void initialise_org_bluez_gdbus_proxy()
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	const char *env_string;
	env_string = g_getenv ("BLUEZ_SRV_DEBUG");
	if (env_string != NULL)
	{
		bluez_service_debug_flags =    g_parse_debug_string (env_string,  bluez_debug_keys,  G_N_ELEMENTS (bluez_debug_keys));
	}
	g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                            "org.bluez",
                            G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                            org_bluez_mng_name_appeared,
                            org_bluez_mng_name_vanished,
                            NULL,
                            NULL);
}

static void org_bluez_start_discovery_name_appeared (GDBusConnection *connection,
                            const gchar    *name,
                            const gchar    *name_owner,
                            gpointer        user_data)

{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
    if(global_adapter_path)
    {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_adapter_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", global_adapter_path, NULL, org_bluez_adapter_path_start_discovery_proxy_created_clb, user_data);
    }
    else
    {
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if(obj != NULL)
		corbridge_control_emit_connection_error(obj, "Scan is initiating ...", "BT is OFF");
    }

}

static void org_bluez_start_discovery_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" org.bluez Adapter Name_vanished \n");
}

void start_bt_scan(const gchar *arg_app_name)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                            "org.bluez",
                            G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                            org_bluez_start_discovery_name_appeared,
                            org_bluez_start_discovery_name_vanished,
                            NULL,
                            NULL);
	disconnect_to_device_found_signal();
	connect_to_device_found_signal();
}






static void org_bluez_stop_discovery_name_appeared (GDBusConnection *connection,
                            const gchar    *name,
                            const gchar    *name_owner,
                            gpointer        user_data)

{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

    if(global_adapter_path)
     {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_adapter_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", global_adapter_path, NULL, org_bluez_adapter_path_stop_discovery_proxy_created_clb, user_data);
     }
    else
     {
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if(obj != NULL)
    	corbridge_control_emit_connection_error(obj, "Stop is initiating ...", "BT is OFF");
     }

}



static void org_bluez_stop_discovery_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
}

void stop_bt_scan(const gchar *arg_app_name)
{
  BLUEZ_DNLD_PRINT(" Function %s\n", __FUNCTION__);
  if (device_found_proxy)
    {
      g_bus_watch_name(G_BUS_TYPE_SYSTEM,
                       "org.bluez",
                       G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                       org_bluez_stop_discovery_name_appeared,
                       org_bluez_stop_discovery_name_vanished,
                       NULL,
                       NULL
                       );
      disconnect_to_device_found_signal();
    }
}

static void create_pair_cb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	gchar *object_path = NULL;
	GError *error = NULL;
	BtInfo *bt_info = (BtInfo *) user_data;

	org_bluez_adapter_call_create_paired_device_finish((OrgBluezAdapter *)source_object, &object_path, res, &error);
	if (error != NULL)
	{
		g_warning(" Error in create pair %s\n", error ? error->message : "no error given.");
		gchar *striped_error = NULL;
		gchar *dup_error = g_strdup(error->message);
		striped_error = g_strrstr(dup_error, ":");

		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);

		if (striped_error == NULL )
		{
			corbridge_control_emit_connection_error(obj, bt_info->address, error->message);
		}
		else
		{
			corbridge_control_emit_connection_error(obj, bt_info->address, striped_error + 1);
		}

		g_error_free(error);
		error = NULL;
		g_free(dup_error);
		dup_error = NULL;
	}
	else
	{
		BLUEZ_DNLD_PRINT(" Pairing is successful %s\n", object_path);
		get_device_properties_and_emit_signal(object_path);
	}
}

static void org_bluez_adapter_create_pair_proxy_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	BtInfo *bt_info = (BtInfo *) user_data;
	OrgBluezAdapter * org_bluez_adapter = NULL;
	org_bluez_adapter = org_bluez_adapter_proxy_new_finish (res, &error);

	 if( (org_bluez_adapter == NULL) || error )
	 {
		g_warning (" Error initializing the org.bluez adapter proxy %s\n", error ? error->message : "no error given.");
		gchar *striped_error = NULL;
		gchar *dup_error = g_strdup(error->message);
		striped_error = g_strrstr(dup_error, ":");

		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);

		if (striped_error == NULL )
		{
			corbridge_control_emit_connection_error(obj, bt_info->address, error->message);
		}
		else
		{
			corbridge_control_emit_connection_error(obj, bt_info->address, striped_error + 1);
		}

		  g_error_free(error);
		  error = NULL;
		  g_free(dup_error);
		  dup_error = NULL;
	 }
	 else
	 {
		//org_bluez_adapter_call_find_device_sync(org_bluez_adapter, bt_info->address, &object_path, NULL, NULL);
		 //g_dbus_proxy_set_default_timeout((GDBusProxy*)org_bluez_adapter, 120000);

		 BLUEZ_DNLD_PRINT(" Arguments to create pair %s = %s\n",bt_info->address, bt_info->capability);
		 if(device_found_proxy)
		 stop_bt_scan("bluetooth");
		 org_bluez_adapter_call_create_paired_device(org_bluez_adapter, bt_info->address, agent_path, bt_info->capability, NULL, create_pair_cb, user_data);

	 }

	 if(org_bluez_adapter)
		 g_object_unref(org_bluez_adapter);
}


static void org_bluez_create_paired_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BtInfo *bt_info = (BtInfo *) user_data;

    if(global_adapter_path)
     {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_adapter_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", global_adapter_path, NULL, org_bluez_adapter_create_pair_proxy_clb, user_data);
     }
    else
    {
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if(obj != NULL)
		corbridge_control_emit_connection_error(obj, bt_info->address, "BT is OFF");
    }

}

static void org_bluez_create_paired_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
}

void free_bt_info(gpointer data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	BtInfo *bt_info = (BtInfo *) data;
	if(bt_info != NULL)
	{
		if(bt_info->address != NULL)
		{
			g_free(bt_info->address);
			bt_info->address = NULL;
		}
		if(bt_info->capability != NULL)
		{
			g_free(bt_info->capability);
			bt_info->capability = NULL;
		}

		g_free(bt_info);
		bt_info = NULL;
	}
}

void create_bt_paired_device(const gchar *arg_address, const gchar *arg_capability, const gchar *arg_pincode)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	//register_for_bluez_agent(arg_capability);
	//new_register_for_bluez_agent(arg_capability);
	//start_org_bluez_agent_service();
	BtInfo *bt_info = g_new0(BtInfo, 1);
	bt_info->address = g_strdup(arg_address);
	bt_info->capability = g_strdup(arg_capability);
	g_hash_table_replace (ghash_table_pincode, g_strdup(arg_address), g_strdup(arg_pincode));

	g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                            "org.bluez",
                            G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                            org_bluez_create_paired_name_appeared,
                            org_bluez_create_paired_name_vanished,
                            bt_info,
                            free_bt_info);


}



static void agent_authorize(OrgBluezAgent *object, GDBusMethodInvocation *invocation, const gchar *arg_device, guint arg_uuid)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT("Device = %s & its UUID = %d\n",arg_device, arg_uuid);
	org_bluez_agent_complete_authorize(object, invocation);
}

static void agent_cancel(OrgBluezAgent *object, GDBusMethodInvocation *invocation)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	org_bluez_agent_complete_cancel(object, invocation);

}

static void agent_confirm_mode_change (OrgBluezAgent *object, GDBusMethodInvocation *invocation, const gchar *arg_mode)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT("Device mode = %s\n", arg_mode);
	org_bluez_agent_complete_confirm_mode_change(object, invocation);
}

static void agent_display_passkey(OrgBluezAgent *object, GDBusMethodInvocation *invocation, const gchar *arg_device, guint arg_passkey, guchar arg_entered)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT("Device = %s & its passkey = %d\n",arg_device, arg_passkey);
	org_bluez_agent_complete_display_passkey(object, invocation);

}

static void agent_display_pin_code(OrgBluezAgent *object, GDBusMethodInvocation *invocation, const gchar *arg_device, const gchar *arg_pincode)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT("Device = %s & its pincode = %s\n",arg_device, arg_pincode);
	org_bluez_agent_complete_display_pin_code(object, invocation);
}

static void agent_release(OrgBluezAgent *object, GDBusMethodInvocation *invocation)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	org_bluez_agent_complete_release(object, invocation);

}

static void agent_request_confirmation(OrgBluezAgent *object, GDBusMethodInvocation *invocation, const gchar *arg_device, guint arg_passkey)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT("Device = %s & its passkey = %d\n",arg_device, arg_passkey);
	//TODO:: Ask for user confirmation by showing passkey of the remote BT device.
	org_bluez_agent_complete_request_confirmation(object, invocation);
}

static void agent_request_passkey(OrgBluezAgent *object, GDBusMethodInvocation *invocation, const gchar *arg_device)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT("Device = %s\n",arg_device);
	org_bluez_agent_complete_request_passkey(object, invocation, 5555);
}

static void agent_request_pin_code(OrgBluezAgent *object, GDBusMethodInvocation *invocation, const gchar *arg_device)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT(" Device = %s\n",arg_device);
	gchar *pincode = NULL;
	gchar *mac_address = NULL;
	gchar *dup_arg_device = g_strdup(arg_device);
	gchar *split_arg_device = g_strrstr (dup_arg_device, "dev_");
	mac_address = g_malloc0(strlen(split_arg_device) + 2);
	replace_string(split_arg_device + 4, mac_address);
	BLUEZ_DNLD_PRINT(" mac address is %s\n", mac_address);

	pincode = g_hash_table_lookup(ghash_table_pincode, mac_address);
	BLUEZ_DNLD_PRINT(" Pincode is %s\n", pincode);
	g_free(mac_address);
	mac_address = NULL;
	g_free(dup_arg_device);
	dup_arg_device = NULL;
	org_bluez_agent_complete_request_pin_code(object, invocation, pincode);
}


static void on_org_bluez_agent_bus_acquired (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;


	org_bluez_agent_proxy = org_bluez_agent_skeleton_new();
	if (NULL != org_bluez_agent_proxy)
	{
		g_signal_connect(org_bluez_agent_proxy, "handle_authorize", G_CALLBACK(agent_authorize), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_cancel", G_CALLBACK(agent_cancel), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_confirm_mode_change", G_CALLBACK(agent_confirm_mode_change), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_display_passkey", G_CALLBACK(agent_display_passkey), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_display_pin_code", G_CALLBACK(agent_display_pin_code), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_release", G_CALLBACK(agent_release), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_request_confirmation", G_CALLBACK(agent_request_confirmation), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_request_passkey", G_CALLBACK(agent_request_passkey), NULL);
		g_signal_connect(org_bluez_agent_proxy, "handle_request_pin_code", G_CALLBACK(agent_request_pin_code), NULL);

		agent_path =  g_strdup_printf("/org/bluez/agent/%d", getpid());
		g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (org_bluez_agent_proxy), connection, agent_path, &error);

			if (error)
			{
			BLUEZ_DNLD_PRINT("\n\n\n");
			BLUEZ_DNLD_PRINT("%s: org.bluez.Agent interface export error\n", __FILE__);
			g_warning("Error in interface export %s\n", error ? error->message : "no error given.");
			g_error_free(error);
			error = NULL;
			BLUEZ_DNLD_PRINT("\n\n\n");
			}
			else
			{
				BLUEZ_DNLD_PRINT(" org.bluez.Agent interface export is successful \n");
			}

	}

}

static void on_org_bluez_agent_name_acquired (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	/* Nothing to do */
}

static void on_org_bluez_agent_name_lost (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	/* free all dbus handlers ... */
}


void start_org_bluez_agent_service()
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	g_bus_own_name (G_BUS_TYPE_SYSTEM,	// bus type
					"org.bluez.Agent",	// interface name/ bus or service name
					G_BUS_NAME_OWNER_FLAGS_NONE,		// bus own flag, can be used to take away the bus and give it to another service
					on_org_bluez_agent_bus_acquired,			// callback invoked when the bus is acquired
					on_org_bluez_agent_name_acquired,			// callback invoked when interface name is acquired
					on_org_bluez_agent_name_lost,				// callback invoked when name is lost to another service or other reason
					NULL,								// user data
					NULL);								// user data free func

}

void replace_string(gchar *orig_str, gchar *mod_str)
{
    int count, i=0;
    int len = strlen(orig_str);
    for (count = 0; count < len; count++)
    {
        if (orig_str[count] == '_' )
        {
            mod_str[i] = ':';
            i++;
        }
        else
        {
            mod_str[i]=orig_str[count];
            i++;
        }
    }
    mod_str[i] = '\0';
}






static void org_bluez_adapter_property_change_proxy_created_clb( GObject *source_object,
                                			 	 GAsyncResult *res,
                                			 	 gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;

	if(global_adapter_proxy)
	{
		g_object_unref(global_adapter_proxy);
		global_adapter_proxy = NULL;
	}

	global_adapter_proxy = org_bluez_adapter_proxy_new_finish (res, &error);

	 if( (global_adapter_proxy == NULL) || error )
	 {
		  g_warning ("Error initializing the org.bluez adapter proxy %s", error ? error->message : "no error given.");
		  g_error_free(error);
		  error = NULL;
	 }
	 else
	 {
		 g_signal_connect(global_adapter_proxy, "property-changed", G_CALLBACK(org_bluez_adapter_property_changed), NULL);
		 g_signal_connect(global_adapter_proxy, "device-removed", G_CALLBACK(org_bluez_adapter_device_removed), NULL);
		 //register_for_bluez_agent("DisplayYesNo");
	 }
}



static void org_bluez_adapter_property_change_name_appeared (GDBusConnection *connection,
                            const gchar    *name,
                            const gchar    *name_owner,
                            gpointer        user_data)

{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

    if(global_adapter_path)
     {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_adapter_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", global_adapter_path, NULL, org_bluez_adapter_property_change_proxy_created_clb, user_data);
     }

}



static void org_bluez_sadapter_property_change_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
}


void create_proxy_for_adapter_property_change()
{

	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                            "org.bluez",
                            G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                            org_bluez_adapter_property_change_name_appeared,
                            org_bluez_sadapter_property_change_name_vanished,
                            NULL,
                            NULL);
}





static void org_bluez_device_properties_proxy_created_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	GError *error_get_properties = NULL;
	GVariant *device_properties = NULL;
	OrgBluezDevice * org_bluez_device = NULL;
	org_bluez_device = org_bluez_device_proxy_new_finish (res, &error);

	 if( (org_bluez_device == NULL) || error )
	 {
		  g_warning ("Error initializing the org.bluez device proxy %s", error ? error->message : "no error given.");
		  g_error_free(error);
		  error = NULL;
	 }
	 else
	 {
		org_bluez_device_call_get_properties_sync(org_bluez_device, &device_properties, NULL, &error_get_properties);
		if (error_get_properties)
		{
			g_warning("Error initializing the org.bluez adapter proxy %s", error_get_properties ? error_get_properties->message : "no error given.");
			g_error_free(error_get_properties);
			error_get_properties = NULL;
		}
		else
		{
			BLUEZ_DNLD_PRINT(" GetProperties is successful \n");
			parse_device_properties(device_properties);
		}
	 }

	 if(org_bluez_device)
		 g_object_unref(org_bluez_device);
}

static void org_bluez_device_properties_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);

    if(user_data)
     {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_device_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", (gchar *)user_data, NULL, org_bluez_device_properties_proxy_created_clb, user_data);
     }

}

static void org_bluez_device_properties_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
}

void get_device_properties_and_emit_signal(const gchar *object_path)
{

	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	gchar *dup_object_path =  g_strdup(object_path);

	g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                            "org.bluez",
                            G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                            org_bluez_device_properties_name_appeared,
                            org_bluez_device_properties_name_vanished,
                            dup_object_path,
                            g_free);
}

gint list_custome_search(gconstpointer a, gconstpointer b)
{
	gchar *success_ptr = NULL;

	success_ptr = g_strrstr((gchar *)a, (gchar *)b );
	if(success_ptr != NULL)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

void service_path_glist_custom_free(gpointer data)
{
	if (data) {
		g_free(data);
		data = NULL;
	}
}

void parse_device_properties(GVariant * device_properties)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GVariantBuilder *builder, *builder1;
	GVariantIter iter, uuid_iter;
	GVariant *value = NULL, *builder1_value = NULL, *builder_value = NULL, *return_properties = NULL;
	gchar *field = NULL;
	gchar *uuid_value = NULL;
	const gchar *uuid;

	if(device_properties)
	{
		builder = g_variant_builder_new(G_VARIANT_TYPE("a{sv}"));

		g_variant_iter_init(&iter, device_properties);

		while (g_variant_iter_next(&iter, "{sv}", &field, &value))
		{
			if((g_strcmp0(field, "UUIDs") == 0) || (g_strcmp0(field, "Name") == 0) || (g_strcmp0(field, "Address") == 0) || (g_strcmp0(field, "Paired") == 0) || (g_strcmp0(field, "Connected") == 0))
			{

				if ((g_strcmp0(field, "UUIDs") == 0))
				{
					builder1 = g_variant_builder_new(G_VARIANT_TYPE("as"));
					g_variant_iter_init(&uuid_iter, value);
					while (g_variant_iter_next(&uuid_iter, "s", &uuid_value))
					{
						uuid = corbridge_bt_convert_uuid_to_string(uuid_value);
						if(uuid == NULL)
							continue;
						if( (g_strcmp0(uuid, "DialupNetworking") == 0)  || (g_strcmp0(uuid, "AudioSource") == 0) || (g_strcmp0(uuid, "IrMCSync") == 0) || (g_strcmp0(uuid, "SIM_Access") == 0) ||
							(g_strcmp0(uuid, "PANU") == 0) || (g_strcmp0(uuid, "NAP") == 0) || (g_strcmp0(uuid, "Phonebook_Access") == 0) || (g_strcmp0(uuid, "OBEXObjectPush") == 0) || (g_strcmp0(uuid, "MAP") == 0) || (g_strcmp0(uuid, "Handsfree") == 0) || (g_strcmp0(uuid, "HandsfreeAudioGateway") == 0) || (g_strcmp0(uuid, "Headset_-_AG") == 0) )
						{
						g_variant_builder_add(builder1, "s", uuid);
						}
					}
					builder1_value = g_variant_builder_end(builder1);
					g_variant_builder_add(builder, "{sv}", field, builder1_value);
				}
				else
				{
					g_variant_builder_add(builder, "{sv}", field, value);
				}
			}
		}

		builder_value = g_variant_builder_end(builder);
		return_properties = g_variant_new("(@a{sv})",builder_value);

	    BluezMng *self = bluez_mgr_get_object();
	    CorbridgeControl *obj = get_corbridge_control_object(self);
	    if(obj != NULL)
	    	corbridge_control_emit_bt_paired_success(obj, return_properties);
	} // if end
}

static void remove_devices_cb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
  GError *error = NULL;
  org_bluez_adapter_call_remove_device_finish((OrgBluezAdapter *) source_object, res, &error);
  if (error)
    {
      g_warning("Error in removing BT device %s", error ? error->message : "no error given.");
      gchar *striped_error = NULL;
      gchar *dup_error = g_strdup(error->message);
      striped_error = g_strrstr(dup_error, ":");
      BluezMng *self = bluez_mgr_get_object();
      CorbridgeControl *obj = get_corbridge_control_object(self);

      if (striped_error == NULL )
        {
    	  corbridge_control_emit_connection_error(obj, (gchar *)user_data, error->message);
        }
      else
        {
          corbridge_control_emit_connection_error(obj, (gchar *)user_data, striped_error + 1);
        }

      g_error_free(error);
      error = NULL;
      g_free(dup_error);
      dup_error = NULL;
    }
  else
    {
      BLUEZ_DNLD_PRINT("Removed Successfully %s\n", (gchar *)user_data);
    }

}

void bt_unpair_device(const gchar *arg_address)
{

  gchar *dup_address = g_strdup(arg_address);
  gchar *object_path = NULL;
  if(global_adapter_proxy)
    {
      org_bluez_adapter_call_find_device_sync(global_adapter_proxy, dup_address, &object_path, NULL, NULL);
      org_bluez_adapter_call_remove_device(global_adapter_proxy, object_path, NULL, remove_devices_cb, dup_address);
    }
  else
    {
      // Generate error signal saying BT is OFF
      BluezMng *self = bluez_mgr_get_object();
      CorbridgeControl *obj = get_corbridge_control_object(self);
      if(obj != NULL)
    	  corbridge_control_emit_connection_error(obj, arg_address, "BT is OFF");
    }

  g_free(dup_address);
  dup_address = NULL;
}


void get_paired_devices()
{
  gchar **device_list = NULL;
  GError *error = NULL;
  gint i = 0;

  BluezMng *self = bluez_mgr_get_object();
  CorbridgeControl *obj = get_corbridge_control_object(self);

    if(global_adapter_proxy)
      {
        org_bluez_adapter_call_list_devices_sync(global_adapter_proxy, &device_list, NULL, &error);
        if(error)
          {

            g_warning (" Error in GetPairedDeviceList %s\n", error ? error->message : "no error given.");
            gchar *striped_error = NULL;
            gchar *dup_error = g_strdup(error->message);
            striped_error = g_strrstr(dup_error, ":");

            if (striped_error == NULL )
            {
            	corbridge_control_emit_connection_error(obj, "Mac address", error->message);
            }
            else
            {
            	    if(g_strrstr(striped_error + 1, "Method \"ListDevices\" with signature \"\" on interface \"org.bluez.Adapter\" doesn't exist"))
            	    	corbridge_control_emit_connection_error(obj, "Mac address", "BT is OFF");
            	    else
            	    	corbridge_control_emit_connection_error(obj, "Mac address", striped_error + 1);
            }

              g_error_free(error);
              error = NULL;
              g_free(dup_error);
              dup_error = NULL;
          }
        else
          {
            BLUEZ_DNLD_PRINT("ListDevices Success \n");
            if(device_list[i] != NULL)
            {
              while (device_list[i])
                {
                  BLUEZ_DNLD_PRINT(" list of Devices %s\n", device_list[i]);
                  get_device_properties_and_emit_signal(device_list[i]);
                  i = i + 1;
                }
            }
            else
              {
            	corbridge_control_emit_connection_error(obj, "Mac address", "No paired List");
              }
          }

      }
    else
      {
        // Generate error signal saying BT is OFF
    	corbridge_control_emit_connection_error(obj,"", "BT is OFF");
      }

}

void send_error_signal(gchar *error_message, gchar *macaddress)
{
	gchar *striped_error = NULL;
	gchar *dup_error = g_strdup(error_message);
	striped_error = g_strrstr(dup_error, ":");
	BluezMng *self = bluez_mgr_get_object();
	CorbridgeControl *obj = get_corbridge_control_object(self);

	if (striped_error == NULL )
	{
		corbridge_control_emit_connection_error(obj, macaddress, error_message);
	}
	else
	{
		corbridge_control_emit_connection_error(obj, macaddress, striped_error + 1);
	}
	g_free(dup_error);
	dup_error = NULL;
}

static void org_bluez_a2dp_proxy_created_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	GError *error_a2dp = NULL;
	OrgBluezAudioSource *org_bluez_a2dp = NULL;
	org_bluez_a2dp = org_bluez_audio_source_proxy_new_finish (res, &error);

	 if( (org_bluez_a2dp == NULL) || error )
	 {
		  g_warning ("Error initializing the org.bluez A2DP proxy %s", error ? error->message : "no error given.");
		  g_error_free(error);
		  error = NULL;
	 }
	 else
	 {
		 org_bluez_audio_source_call_connect_sync(org_bluez_a2dp, NULL, &error_a2dp);
		if(error_a2dp)
		{
			g_warning("Error while calling A2DP connect method %s", error_a2dp ? error_a2dp->message : "no error given.");
			send_error_signal(error_a2dp->message, (gchar *)user_data);
			g_error_free(error_a2dp);
			error_a2dp = NULL;
		}
		else
		{
			BLUEZ_DNLD_PRINT(" A2DP connection is successful \n");
			BluezMng *self = bluez_mgr_get_object();
			CorbridgeControl *obj = get_corbridge_control_object(self);
			if(obj != NULL)
			corbridge_control_emit_device_info(obj, (gchar *)user_data, "Audio Source Connected to Device");
		}
	 }

	 if(org_bluez_a2dp)
		 g_object_unref(org_bluez_a2dp);
}


static void org_bluez_a2dp_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	gchar *object_path = NULL;

    if(user_data)
     {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_adapter_call_find_device_sync(global_adapter_proxy, (gchar *)user_data, &object_path, NULL, NULL);
    	org_bluez_audio_source_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", object_path, NULL, org_bluez_a2dp_proxy_created_clb, user_data);
     }
}

static void org_bluez_a2dp_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
}

void trigger_a2dp_connect(const gchar *address)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT(" A2DP connecting for BT device %s \n", address);
	gchar *dup_address = g_strdup(address);
	if (global_adapter_proxy)
	{
		g_bus_watch_name (G_BUS_TYPE_SYSTEM,
	                      "org.bluez",
	                       G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
	                       org_bluez_a2dp_name_appeared,
	                       org_bluez_a2dp_name_vanished,
	                       dup_address,
	                       g_free);
	}
	else
	{
		// Generate error signal saying BT is OFF
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if(obj != NULL)
			corbridge_control_emit_connection_error(obj, address, "BT is OFF");
	}

	//g_free(dup_address);
	//dup_address = NULL; Freed by DBUS on closer of "org.bluez" bus
}

static void org_bluez_a2dp_disconnect_proxy_created_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	GError *error_a2dp = NULL;
	OrgBluezAudioSource *org_bluez_a2dp = NULL;
	org_bluez_a2dp = org_bluez_audio_source_proxy_new_finish (res, &error);

	 if( (org_bluez_a2dp == NULL) || error )
	 {
		  g_warning ("Error initializing the org.bluez A2DP proxy %s", error ? error->message : "no error given.");
		  g_error_free(error);
		  error = NULL;
	 }
	 else
	 {
		 org_bluez_audio_source_call_disconnect_sync(org_bluez_a2dp, NULL, &error_a2dp);
		if(error_a2dp)
		{
			g_warning("Error while calling A2DP connect method %s", error_a2dp ? error_a2dp->message : "no error given.");
			send_error_signal(error_a2dp->message, (gchar *)user_data);
			g_error_free(error_a2dp);
			error_a2dp = NULL;
		}
		else
		{
			BLUEZ_DNLD_PRINT(" A2DP disconnection is successful \n");
			BluezMng *self = bluez_mgr_get_object();
			CorbridgeControl *obj = get_corbridge_control_object(self);
			if(obj != NULL)
				corbridge_control_emit_device_info(obj, (gchar *)user_data, "Audio Source Disconnected from Device");
		}
	 }

	 if(org_bluez_a2dp)
		 g_object_unref(org_bluez_a2dp);
}


static void org_bluez_a2dp_disconnect_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	gchar *object_path = NULL;

    if(user_data)
     {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_adapter_call_find_device_sync(global_adapter_proxy, (gchar *)user_data, &object_path, NULL, NULL);
    	org_bluez_audio_source_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", object_path, NULL, org_bluez_a2dp_disconnect_proxy_created_clb, user_data);
     }
}

static void org_bluez_a2dp_disconnect_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
}


void trigger_a2dp_disconnect(const gchar *address)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT(" A2DP disconnecting for BT device %s \n", address);
	gchar *dup_address = g_strdup(address);
	if (global_adapter_proxy)
	{
		g_bus_watch_name (G_BUS_TYPE_SYSTEM,
	                      "org.bluez",
	                       G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
	                       org_bluez_a2dp_disconnect_name_appeared,
	                       org_bluez_a2dp_disconnect_name_vanished,
	                       dup_address,
	                       g_free);
	}
	else
	{
		// Generate error signal saying BT is OFF
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if(obj != NULL)
			corbridge_control_emit_connection_error(obj, address, "BT is OFF");
	}
}
static void org_bluez_a2dp_get_property_proxy_created_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GError *error = NULL;
	GError *error_a2dp = NULL;
	GVariant *pA2dpProperty = NULL;
	OrgBluezAudioSource *org_bluez_a2dp = NULL;
	org_bluez_a2dp = org_bluez_audio_source_proxy_new_finish (res, &error);

	 if( (org_bluez_a2dp == NULL) || error )
	 {
		  g_warning ("Error initializing the org.bluez A2DP proxy %s", error ? error->message : "no error given.");
		  g_error_free(error);
		  error = NULL;
	 }
	 else
	 {
		 org_bluez_audio_source_call_get_properties_sync (org_bluez_a2dp, &pA2dpProperty, NULL, &error_a2dp);

		if(error_a2dp)
		{
			g_warning("Error while calling A2DP get property method %s", error_a2dp ? error_a2dp->message : "no error given.");
			send_error_signal(error_a2dp->message, (gchar *)user_data);
			g_error_free(error_a2dp);
			error_a2dp = NULL;
		}
		else
		{
			GVariantIter viter;
			GVariant *pA2dpState = NULL;
			gchar *pProperty  = NULL;
			BluezMng *self = bluez_mgr_get_object();
			CorbridgeControl *obj = get_corbridge_control_object(self);

			g_variant_iter_init (&viter, pA2dpProperty);
			while (g_variant_iter_next(&viter, "{sv}", &pProperty, &pA2dpState)) {

			         if (g_strcmp0 (pProperty, "State") == 0){

			        	 if (g_strcmp0 (g_variant_get_string(pA2dpState, NULL), "connected") == 0 ||
			        		 g_strcmp0 (g_variant_get_string(pA2dpState, NULL), "playing") == 0) {
			        		 BLUEZ_DNLD_PRINT("****Connected****\n");
			        		 corbridge_control_emit_device_info(obj, (gchar *)user_data, "DeviceStateConnected");
			        	 }
			        	 else {
			        		 BLUEZ_DNLD_PRINT("****Disonnected****\n");
			        		 corbridge_control_emit_device_info(obj, (gchar *)user_data, "DeviceStateDisconnected");
			        	 }
			         }
			}

			BLUEZ_DNLD_PRINT(" A2DP Get Property Call successful \n");

		}
	 }

	 if(org_bluez_a2dp)
		 g_object_unref(org_bluez_a2dp);
}

static void org_bluez_a2dp_get_property_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	gchar *object_path = NULL;

    if(user_data)
     {
    	BLUEZ_DNLD_PRINT(" Adapter path = %s in function %s \n", global_adapter_path, __FUNCTION__);
    	org_bluez_adapter_call_find_device_sync(global_adapter_proxy, (gchar *)user_data, &object_path, NULL, NULL);
    	org_bluez_audio_source_proxy_new( connection, G_DBUS_CALL_FLAGS_NONE, "org.bluez", object_path, NULL, org_bluez_a2dp_get_property_proxy_created_clb, user_data);
     }
}

static void org_bluez_a2dp_get_property_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
}

void a2dp_get_properties(const gchar *address)
{
	BLUEZ_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUEZ_DNLD_PRINT(" A2DP Connect/Disconnect Property for BT device %s \n", address);
	gchar *dup_address = g_strdup(address);
	if (global_adapter_proxy)
	{
		g_bus_watch_name (G_BUS_TYPE_SYSTEM,
	                      "org.bluez",
	                       G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
	                       org_bluez_a2dp_get_property_name_appeared,
	                       org_bluez_a2dp_get_property_name_vanished,
	                       dup_address,
	                       g_free);

	}
	else
	{
		// Generate error signal saying BT is OFF
		BluezMng *self = bluez_mgr_get_object();
		CorbridgeControl *obj = get_corbridge_control_object(self);
		if(obj != NULL)
			corbridge_control_emit_connection_error(obj, address, "BT is OFF");
	}
}

void set_property_to_onoff_bluetooth(const gboolean value)
{
	//TODO:: please complete the steps
	//org_bluez_adapter_call_set_property_sync();
}
