/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SYNCEVO_CLIENT_H_
#define SYNCEVO_CLIENT_H_

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

typedef enum {

	BLUETOOTHSYNC_SRV_DEBUG          = 1 << 0,

}BluetoothSyncSrvDebugFlag;
#define BLUETOOTHSYNC_SRV_HAS_DEBUG               ((bluetooth_sync_service_debug_flags) & 1)
#define BLUETOOTHSYNC_DNLD_PRINT( a ...) \
    if (G_LIKELY (BLUETOOTHSYNC_SRV_HAS_DEBUG )) \
    {                                   \
        g_print("Bluetooth Sync Service::> " a);                \
    }
extern guint bluetooth_sync_service_debug_flags;

void error_cb(const gchar *error_message, const gchar *mac_aaddress);
void complete_cb(const gchar *mac_aaddress);
/*
 * Requests a certain synchronization mode when initiating a sync.
 * Available sync options are mentioned below
*/
#define ONEWAYSYNC "one-way"
#define TWOWAYSYNC "two-way"
#define SLOW "slow"
#define REFRESHFROMREMOTE "refresh-from-remote"
#define REFRESHFROMLOCAL "refresh-from-local"
#define ONEWAYFROMREMOTE "one-way-from-remote"
#define ONEWAYFROMLOCAL "one-way-from-local"
#define LOCALCACHESLOW "local-cache-slow"
#define SYNCMODEDISABLED "disabled"

//pbap profile configuration for client required for syncevo
#define CLIENT_CONFIG_BACKEND 				"addressbook"
#define CLIENT_CONFIG_URI 					"addressbook"
#define CLIENT_CONFIG_USERNAME				"corbridge"
#define	CLIENT_CONFIG_PBAP_PROFILE 			"local://@pbap"
#define	CLIENT_CONFIG_PEER_IS_CLIENT 		"1"
#define	CLIENT_CONFIG_KEYRING				"no"
#define	CLIENT_CONFIG_ICONURI				"image://themedimage/icons/services/syncevolution"
#define	CLIENT_CONFIG_CONSUMER_READY		"1"
#define	CLIENT_CONFIG_PASSWORD				"test"
#define	CLIENT_CONFIG_NAME					"pbap"

//pbap profile configuration for remoted device required for syncevo
#define	SERVER_CONFIG_BACKEND				"PBAP Address Book"
#define	SERVER_CONFIG_SYNCURL				""
#define	SERVER_CONFIG_KEYRING				"no"
#define	SERVER_CONFIG_NAME					"target-config@pbap"
#define	SERVER_CONFIG_



typedef struct _SyncevoInfo
{
	gint maxcount;
	gint count;
	gchar *message;
} SyncevoInfo;



void initialise_org_syncevolution_gdbus_proxy();
void get_contacts_from_syncevolution(const gchar *mac_address);
gboolean get_sync_status_syncevo(const gchar *mac_address);
void cancel_inprogress_request_syncevo(const gchar *arg_mac_address);
void error_cb(const gchar *error_message, const gchar *mac_aaddress);


#endif /* SYNCML_CLIENT_H_ */
