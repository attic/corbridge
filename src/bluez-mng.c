/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "bluez-mng.h"
#include "org_bluez/bluez_interface.h"
#include "syncevo_client.h"

G_DEFINE_TYPE (BluezMng, bluez_mng, G_TYPE_OBJECT)

#define MNG_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), BLUEZ_TYPE_MNG, BluezMngPrivate))

struct _BluezMngPrivate
{
	CorbridgeControl *corbridge_control_obj;
	CorbridgeAddressBook *address_book_obj;
	guint name_owner_id;
};


static void bluez_mgr_termination_handler(int signum) {
	g_print("\n bluez_mgr_termination_handler \n");
	//TODO:: clean the bluetooth service here
	exit(0);
}

static void bluez_mgr_register_exit_functionalities() {
	struct sigaction action;
	action.sa_handler = bluez_mgr_termination_handler;
	action.sa_flags = 0;
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGHUP, &action, NULL);
	sigaction(SIGTERM, &action, NULL);
}


static void
bluez_mng_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bluez_mng_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bluez_mng_dispose (GObject *object)
{
  G_OBJECT_CLASS (bluez_mng_parent_class)->dispose (object);
}

static void
bluez_mng_finalize (GObject *object)
{
  G_OBJECT_CLASS (bluez_mng_parent_class)->finalize (object);
}

static void
bluez_mng_class_init (BluezMngClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (BluezMngPrivate));

  object_class->get_property = bluez_mng_get_property;
  object_class->set_property = bluez_mng_set_property;
  object_class->dispose = bluez_mng_dispose;
  object_class->finalize = bluez_mng_finalize;
}


static void name_acquired_handler_cb(GDBusConnection *connection,
		const gchar *name, gpointer user_data) {
}

static void name_lost_handler_cb(GDBusConnection *connection, const gchar *name,
		gpointer user_data) {
	/* free all dbus handlers ... */
}


static void corbridge_start_bt_scan (CorbridgeControl *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	start_bt_scan(arg_app_name);
	corbridge_control_complete_start_bt_scan (object, invocation);
}


static void corbridge_stop_bt_scan (CorbridgeControl *object, GDBusMethodInvocation *invocation, const gchar *arg_app_name)
{
	stop_bt_scan(arg_app_name);
	corbridge_control_complete_stop_bt_scan (object, invocation);
}


static void corbridge_create_bt_paired_device(CorbridgeControl *object,
	    						 GDBusMethodInvocation *invocation,
	    						 const gchar *arg_address,
	    						 const gchar *arg_capability,
	    						 const gchar *arg_pincode
	    						 )
{
	create_bt_paired_device(arg_address, arg_capability, arg_pincode);
	corbridge_control_complete_create_bt_paired_device(object, invocation);
}


static void corbridge_bt_network_remove (CorbridgeControl *object, GDBusMethodInvocation *invocation, const gchar *arg_address)
{
  bt_unpair_device(arg_address);
  corbridge_control_complete_bt_network_remove(object, invocation);
}


static void corbridge_get_bt_paired_device_list(CorbridgeControl *object, GDBusMethodInvocation *invocation)
{
  get_paired_devices();
  //@paired_list: List of paired devices and its service object path and its profile info
  //<arg name="paired_list" direction="out" type="(a(oa{sv}))"/>
  corbridge_control_complete_get_bt_paired_device_list(object, invocation);
}


static void corbridge_a2dp_connect(CorbridgeControl *object, GDBusMethodInvocation *invocation, const gchar *arg_address)
{
	trigger_a2dp_connect(arg_address); // MAke sync request. On failure error signal will be emitted
	corbridge_control_complete_a2dp_connect(object, invocation);
}

static void corbridge_a2dp_disconnect(CorbridgeControl *object, GDBusMethodInvocation *invocation, const gchar *arg_address)
{
	trigger_a2dp_disconnect(arg_address); // MAke sync request. On failure error signal will be emitted
	corbridge_control_complete_a2dp_disconnect(object, invocation);
}

static void corbridge_a2dp_get_property(CorbridgeControl *object, GDBusMethodInvocation *invocation, const gchar *arg_address)
{
	a2dp_get_properties(arg_address); // MAke sync request. On failure error signal will be emitted
	corbridge_control_complete_a2dp_get_property(object, invocation);
}

static void corbridge_bt_on_off(CorbridgeControl *object, GDBusMethodInvocation *invocation, const gboolean value)
{
	set_property_to_onoff_bluetooth(value);
	corbridge_control_complete_technology_powered(object, invocation);
}

/****   Address book  **/
static void get_contacts(CorbridgeAddressBook *object, GDBusMethodInvocation *invocation, const gchar *arg_mac_address, const gchar *arg_bt_profile)
{
	if( (g_strcmp0(arg_bt_profile, "") == 0) || (g_strcmp0(arg_bt_profile, PBAPPROFILE) == 0) )
	{
		get_contacts_from_syncevolution(arg_mac_address);
	}
	else
	{
		BLUETOOTHSYNC_DNLD_PRINT("SyncMl Profile called\n");
		gchar *profile = g_strdup_printf("%s profile is not Implemented",arg_bt_profile);
		error_cb(profile, arg_mac_address);
		g_free(profile);
		profile = NULL;
	}

	corbridge_address_book_complete_get_contacts(object, invocation);
}

/**
 * Description: To cancel the ongoing addressbook sync request for the mentioned device
 * object: CorbridgeAddressBook proxy
 * invocation: invocation
 * arg_mac_address: Mac address of the device
 */
static void cancel_the_request(CorbridgeAddressBook *object, GDBusMethodInvocation *invocation, const gchar *arg_mac_address)
{
	cancel_inprogress_request_syncevo(arg_mac_address);
	corbridge_address_book_complete_cancel(object, invocation);
}

static void get_sync_status(CorbridgeAddressBook *object, GDBusMethodInvocation *invocation, const gchar *arg_mac_address)
{
	gboolean status = get_sync_status_syncevo(arg_mac_address);
	corbridge_address_book_complete_get_sync_status(object, invocation, status);
}

/***********/
static void
bus_acquired_handler_cb (GDBusConnection *connection, const gchar *name,
                         gpointer user_data)
{
  GError *error = NULL;
  BluezMng *self = (BluezMng *) user_data;
  if (!connection || error != NULL)
    return;
  bluez_mgr_register_exit_functionalities ();
  self->priv = MNG_PRIVATE(self);
  self->priv->corbridge_control_obj = corbridge_control_skeleton_new ();
  if (!self->priv->corbridge_control_obj)
    return;

  g_signal_connect (self->priv->corbridge_control_obj, "handle_start_bt_scan",
                    G_CALLBACK (corbridge_start_bt_scan), NULL);
  g_signal_connect (self->priv->corbridge_control_obj, "handle_stop_bt_scan",
                    G_CALLBACK (corbridge_stop_bt_scan), NULL);
  g_signal_connect (self->priv->corbridge_control_obj,
                    "handle_create_bt_paired_device",
                    G_CALLBACK (corbridge_create_bt_paired_device), NULL);
  g_signal_connect (self->priv->corbridge_control_obj,
                    "handle_bt_network_remove",
                    G_CALLBACK (corbridge_bt_network_remove), NULL);
  g_signal_connect (self->priv->corbridge_control_obj,
                    "handle_get_bt_paired_device_list",
                    G_CALLBACK (corbridge_get_bt_paired_device_list), NULL);
  g_signal_connect (self->priv->corbridge_control_obj, "handle_a2dp_connect",
                    G_CALLBACK (corbridge_a2dp_connect), NULL);
  g_signal_connect (self->priv->corbridge_control_obj, "handle_a2dp_disconnect",
                    G_CALLBACK (corbridge_a2dp_disconnect), NULL);
  g_signal_connect (self->priv->corbridge_control_obj,
                    "handle_a2dp_get_property",
                    G_CALLBACK (corbridge_a2dp_get_property), NULL);
  g_signal_connect (self->priv->corbridge_control_obj,
                    "handle_technology_powered",
                    G_CALLBACK (corbridge_bt_on_off), NULL);

  if (!g_dbus_interface_skeleton_export (
      G_DBUS_INTERFACE_SKELETON (self->priv->corbridge_control_obj), connection,
      "/org/apertis/Corbridge/Control", NULL))
    g_print ("%s: Bluetooth control Interface export error \n", __FILE__);

  /****   Address Book Proxy for contact Sync       ***/
  self->priv->address_book_obj = corbridge_address_book_skeleton_new ();
  if (!self->priv->address_book_obj)
    return;
  g_signal_connect (self->priv->address_book_obj, "handle-get-contacts",
                    G_CALLBACK (get_contacts), NULL);
  g_signal_connect (self->priv->address_book_obj, "handle-cancel",
                    G_CALLBACK (cancel_the_request), NULL);
  g_signal_connect (self->priv->address_book_obj, "handle-get-sync-status",
                    G_CALLBACK (get_sync_status), NULL);
  if (!g_dbus_interface_skeleton_export (
      G_DBUS_INTERFACE_SKELETON (self->priv->address_book_obj), connection,
      "/org/apertis/Corbridge/AddressBook", NULL))
    BLUETOOTHSYNC_DNLD_PRINT(
        "%s: Address Book Control interface export error\n", __FILE__);
}

static void
bluez_mng_init (BluezMng *self)
{
  self->priv = MNG_PRIVATE(self);
  self->priv->name_owner_id = g_bus_own_name (
      G_BUS_TYPE_SESSION, "org.apertis.Corbridge",
      G_BUS_NAME_OWNER_FLAGS_NONE, bus_acquired_handler_cb,
      name_acquired_handler_cb, name_lost_handler_cb, self, NULL);
  initialise_org_bluez_gdbus_proxy ();
  initialise_org_syncevolution_gdbus_proxy ();
}

BluezMng *
bluez_mng_new (void)
{
  return g_object_new (BLUEZ_TYPE_MNG, NULL);
}

CorbridgeControl *get_corbridge_control_object(BluezMng *obj)
{
	if(obj == NULL)
	return NULL;
	return obj->priv->corbridge_control_obj;
}

CorbridgeAddressBook *get_corbridge_address_book_object(BluezMng *obj)
{
	if(obj == NULL)
	return NULL;
	return obj->priv->address_book_obj;
}

BluezMng *
bluez_mgr_get_object(void) {
	static BluezMng *self = NULL;
	if (NULL == self) {
		self = g_object_new(BLUEZ_TYPE_MNG, NULL);
	}
	return self;
}

gint main(gint argc, gchar *argv[]) {
	GMainLoop *loop = g_main_loop_new(NULL, FALSE);
	BluezMng *bluez_mgr = bluez_mgr_get_object();
	g_main_loop_run(loop);

	if (bluez_mgr->priv->name_owner_id != 0)
		g_bus_unown_name(bluez_mgr->priv->name_owner_id);

	return 0;
}


