/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "bluez-mng.h"
#include "syncevo_client.h"
#include "syncevo_server_gdbus_fi.h"
#include "syncevo_session_gdbus_fi.h"
#include <string.h>
#include <ctype.h>

static void start_session_callback(GObject *source_object, GAsyncResult *res, gpointer user_data);

OrgSyncevolutionServer * server_proxy;
OrgSyncevolutionSession *session_proxy;
gboolean isSessionPBAP = FALSE;
GHashTable *ghash_table_session_path;
GHashTable *ghash_table_info;

guint  bluetooth_sync_service_debug_flags = 0;

static const GDebugKey bluetooth_sync_debug_keys[] = {
  { "bluetooth-sync", BLUETOOTHSYNC_SRV_DEBUG }
};

/**
 * Description: On failure of GetContacts and Cancel method this signal will be called with appropriate error message
 * error_message: Error info
 * mac_aaddress: Mac address of the device.
 */
void error_cb(const gchar *error_message, const gchar *mac_aaddress)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Error signal called for the mac address %s with the error message %s \n", mac_aaddress, error_message);
	gchar *custome_error = NULL;
	BluezMng *self = bluez_mgr_get_object();
	CorbridgeAddressBook *obj = get_corbridge_address_book_object(self);

	if((g_strcmp0(error_message, "Transport got disconnected")) == 0 )
	{
		custome_error = g_strdup("Sync operation Failed");
		if(obj != NULL)
			corbridge_address_book_emit_error(obj, custome_error, mac_aaddress);
		g_free(custome_error);
		custome_error = NULL;
	}
	else if((g_strcmp0(error_message, " Connection refused (111)")) == 0 )
	{
		custome_error = g_strdup("Connection refused by peer");
		if(obj != NULL)
			corbridge_address_book_emit_error(obj, custome_error, mac_aaddress);
		g_free(custome_error);
		custome_error = NULL;
	}
	else
	{
		corbridge_address_book_emit_error(obj, error_message, mac_aaddress);
	}
}

void replace_colon_with_underscore(gchar *orig_str, gchar *mod_str)
{
    int count, i=0;
    int len = strlen(orig_str);
    for (count = 0; count < len; count++)
    {
        if (orig_str[count] == ':' )
        {
         mod_str[i] = '_';
        i++;
        }
        else
        {
            mod_str[i]=tolower(orig_str[count]);
            i++;
        }
    }
    mod_str[i] = '\0';
}

static void ghashtable_free(gpointer pvalue)
{
	if (pvalue)
	{
		g_free(pvalue);
		pvalue = NULL;
	}
}
static void custome_ghashtable_value_free(gpointer pvalue)
{
	SyncevoInfo *syncevo_info = (SyncevoInfo *)pvalue;
	if(syncevo_info)
	{
		if(syncevo_info->message)
		{
			g_free(syncevo_info->message);
			syncevo_info->message = NULL;
		}
		g_free(syncevo_info);
		syncevo_info = NULL;
	}

}

static gboolean g_print_mac_existance_ghash(gpointer key, gpointer value, gpointer user_data)
{

	BLUETOOTHSYNC_DNLD_PRINT(" key %s --> value %s \n", (gchar *)key, (gchar *)value);
	if(g_strcmp0( (gchar *)value, (gchar *)user_data) == 0)
		{
			return TRUE;
		}
	else
		return FALSE;

}

static gboolean g_print_mac_remove_ghash(gpointer key, gpointer value, gpointer user_data)
{

	BLUETOOTHSYNC_DNLD_PRINT(" key %s --> value %s and user data %s \n", (gchar *)key, (gchar *)value, (gchar *)user_data);
	if(g_strcmp0( (gchar *)value, (gchar *)user_data) == 0)
		{
			BLUETOOTHSYNC_DNLD_PRINT("Removed the entry for %s\n", (gchar *)user_data);
			return TRUE;
		}
	else
		return FALSE;
}

#if 0
static void g_print_ghash(gpointer key, gpointer value, gpointer user_data)
{
	BLUETOOTHSYNC_DNLD_PRINT(" key %s --> value %s \n key address %p--> value address %p \n", (gchar *)key, (gchar *)value, key, value);
}
#endif

static void g_hash_for_session_path_init()
{
	ghash_table_session_path = g_hash_table_new_full(g_str_hash, g_str_equal, ghashtable_free, ghashtable_free);
	ghash_table_info = g_hash_table_new_full(g_str_hash, g_str_equal, ghashtable_free, custome_ghashtable_value_free);
}


static void parse_and_send_error_signal(const gchar *error_message, gpointer user_data)
{
	gchar *striped_error = NULL;
	gchar *dup_error = g_strdup(error_message);
	striped_error = g_strrstr(dup_error, ":");
	if (striped_error == NULL )
	{
		error_cb(error_message, (gchar *)user_data);
	}
	else
	{
		error_cb(striped_error + 1, (gchar *)user_data);
	}
	g_free(dup_error);
	dup_error = NULL;
}


static void g_hash_table_free(gpointer mac_address)
{
	int i = 0;
	int n = g_hash_table_size(ghash_table_session_path);
	for(i = 0; i< n; i++)
	{
		g_hash_table_foreach_remove(ghash_table_session_path, g_print_mac_remove_ghash, mac_address);
	}
}
/**
 * description: sets the client side configurations.
 */
void call_set_config(gpointer user_data)
{
	 GError *error = NULL;
	 gboolean arg_update = FALSE;
	 gboolean arg_temporary = FALSE;
	 srand(time(NULL));
	 int r = rand();
	 gchar *device_id = g_strdup_printf("syncevolution-obef%d",r);
	 gchar *sync_url = g_strdup_printf("local://@%s", (gchar *)user_data);

	 GVariantBuilder *builder_config, *builder_address, *builder_empty;
	 GVariant *value_address = NULL, *value_config = NULL, *value_empty = NULL;
	 builder_config = g_variant_builder_new(G_VARIANT_TYPE("a{sa{ss}}"));

	 builder_address = g_variant_builder_new(G_VARIANT_TYPE("a{ss}"));
	 g_variant_builder_add(builder_address, "{ss}", "backend", CLIENT_CONFIG_BACKEND);
	 g_variant_builder_add(builder_address, "{ss}", "sync", ONEWAYSYNC);
	 g_variant_builder_add(builder_address, "{ss}", "uri", CLIENT_CONFIG_URI);
	 g_variant_builder_add(builder_address, "{ss}", "database", (gchar *)user_data);
	 value_address = g_variant_new("{sa{ss}}", "source/addressbook", builder_address);

	 builder_empty = g_variant_builder_new(G_VARIANT_TYPE("a{ss}"));
	 g_variant_builder_add(builder_empty, "{ss}", "username", CLIENT_CONFIG_USERNAME);
	 //g_variant_builder_add(builder_empty, "{ss}", "syncURL", CLIENT_CONFIG_PBAP_PROFILE);
	 g_variant_builder_add(builder_empty, "{ss}", "syncURL", sync_url);
	 g_variant_builder_add(builder_empty, "{ss}", "PeerIsClient", CLIENT_CONFIG_PEER_IS_CLIENT);
	 g_variant_builder_add(builder_empty, "{ss}", "remoteDeviceId", "syncevolution-0b6ef69b-bd67-4253-9df5-723d61da43e2");
	 g_variant_builder_add(builder_empty, "{ss}", "keyring", CLIENT_CONFIG_KEYRING);
	 g_variant_builder_add(builder_empty, "{ss}", "IconURI", CLIENT_CONFIG_ICONURI);
	 //g_variant_builder_add(builder_empty, "{ss}", "deviceId", "syncevolution-6d97db0e-d089-4383-9871-2bebfd02a3ee");
	 g_variant_builder_add(builder_empty, "{ss}", "deviceId", device_id);
	 g_variant_builder_add(builder_empty, "{ss}", "ConsumerReady", CLIENT_CONFIG_CONSUMER_READY);
	 g_variant_builder_add(builder_empty, "{ss}", "password", CLIENT_CONFIG_PASSWORD);
	 g_variant_builder_add(builder_empty, "{ss}", "configName", CLIENT_CONFIG_NAME);

	 value_empty = g_variant_new("{sa{ss}}", "", builder_empty);

	 g_variant_builder_add_value(builder_config, value_empty);
	 g_variant_builder_add_value(builder_config, value_address);
	 //g_variant_builder_add_value(builder_config, value_address);
	//value_config = g_variant_new("a{sa{ss}}", g_variant_builder_end(builder_config));
	 value_config = g_variant_builder_end(builder_config);
	 org_syncevolution_session_call_set_config_sync(session_proxy, arg_update, arg_temporary, value_config, NULL, &error);
	 if( error )
	 {
		g_warning("Error in SetConfig method %s", error ? error->message : "no error given.");
		error_cb(error->message, (gchar *)user_data);
		g_error_free(error);
		error = NULL;
	 }
	 else
	 {
		 BLUETOOTHSYNC_DNLD_PRINT("\n SetConfig success \n");
	 }
}


/**
 * description: sets the target side configurations.
 */
void call_set_target_config(gpointer user_data)
{
	 BLUETOOTHSYNC_DNLD_PRINT(" Setting target conf for the mac address %s \n", (gchar *)user_data);

	 GError *error = NULL, *error_detach = NULL;
	 gboolean arg_update = FALSE;
	 gboolean arg_temporary = FALSE;

	 GVariantBuilder *builder_config, *builder_address, *builder_empty;
	 GVariant *value_address = NULL, *value_empty = NULL, *value_config = NULL;
	 builder_config = g_variant_builder_new(G_VARIANT_TYPE("a{sa{ss}}"));

	 builder_address = g_variant_builder_new(G_VARIANT_TYPE("a{ss}"));
	 gchar *database_location = g_strdup_printf("obex-bt://%s", (gchar *)user_data);
	 g_variant_builder_add(builder_address, "{ss}", "database", database_location);
	 g_variant_builder_add(builder_address, "{ss}", "sync", ONEWAYSYNC);//Todo: only one way sync support
	 //g_variant_builder_add(builder_address, "{ss}", "uri", "addressbook"); // remove this
	 g_variant_builder_add(builder_address, "{ss}", "backend", SERVER_CONFIG_BACKEND); // change name as PBAP Address Book instead of addressbook
	 value_address = g_variant_new("{sa{ss}}", "source/addressbook", builder_address);

	 builder_empty = g_variant_builder_new(G_VARIANT_TYPE("a{ss}"));
	 g_variant_builder_add(builder_empty, "{ss}", "syncURL", SERVER_CONFIG_SYNCURL);
	 g_variant_builder_add(builder_empty, "{ss}", "keyring", SERVER_CONFIG_KEYRING);
	 g_variant_builder_add(builder_empty, "{ss}", "configName", SERVER_CONFIG_NAME);
	 g_variant_builder_add(builder_empty, "{ss}", "deviceId", "syncevolution-0b6ef69b-bd67-4253-9df5-723d61da43e2");
	 value_empty = g_variant_new("{sa{ss}}", "", builder_empty);

	 g_variant_builder_add_value(builder_config, value_empty);
	 g_variant_builder_add_value(builder_config, value_address);
	//value_config = g_variant_new("a{sa{ss}}", g_variant_builder_end(builder_config));
	 value_config = g_variant_builder_end(builder_config);

	 org_syncevolution_session_call_set_config_sync(session_proxy, arg_update, arg_temporary, value_config, NULL, &error);
	 if( error )
	 {
		g_warning("Error in SetConfig method %s", error ? error->message : "no error given.");
		error_cb(error->message, (gchar *)user_data);
		g_error_free(error);
		error = NULL;
	 }
	 else
	 {
		 BLUETOOTHSYNC_DNLD_PRINT("\n Target SetConfig success \n");
		 isSessionPBAP = TRUE;
		 org_syncevolution_session_call_detach_sync(session_proxy, NULL, &error_detach);
		 if( error_detach )
			 {
				g_warning("Error in Detach method %s", error_detach ? error_detach->message : "no error given.");
				g_error_free(error_detach);
				error_detach = NULL;
			 }
			 else
			 {
				 BLUETOOTHSYNC_DNLD_PRINT("Detach sucess and calling new session \n");
				 gchar *mac_address = g_strdup((gchar *)user_data);
				 //org_syncevolution_server_call_start_session(server_proxy, "pbap", NULL, start_session_callback, (gpointer)mac_address);
				 org_syncevolution_server_call_start_session(server_proxy, mac_address, NULL, start_session_callback, (gpointer)mac_address);
			 }
	 }
	 g_free(database_location);
	 database_location = NULL;
}

static void status_changed_signal(OrgSyncevolutionSession *object, const gchar *arg_status, guint arg_error, GVariant *arg_sources)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUETOOTHSYNC_DNLD_PRINT(" Status changed %s is %d \n", arg_status, arg_error);
	BLUETOOTHSYNC_DNLD_PRINT(" Status changed GVariant %s\n", g_variant_print(arg_sources, 1));
}

static void progress_changed_signal(OrgSyncevolutionSession *object, gint arg_progress, GVariant *arg_sources)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUETOOTHSYNC_DNLD_PRINT(" Progress changed %d GVariant = %s \n", arg_progress, g_variant_print(arg_sources, 1));
}

static void sync_callback(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	GError *error = NULL;
	org_syncevolution_session_call_sync_finish((OrgSyncevolutionSession *)source_object, res, &error);
	 if( error )
	 {
		g_warning("Error in sync method %s", error ? error->message : "no error given.");
		g_error_free(error);
		error = NULL;
	 }
	 else
	 {
		BLUETOOTHSYNC_DNLD_PRINT(" sync method success on session path = %s \n", (gchar *)user_data);
	 }
}

static void org_syncevolution_session_callback(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	GError *error = NULL, *error_getname = NULL;
	session_proxy = org_syncevolution_session_proxy_new_finish(res, &error);
	 if( (session_proxy == NULL) || error )
	 {
		g_warning("Error initializing the org.syncevolution.session proxy %s", error ? error->message : "no error given.");
		g_error_free(error);
		error = NULL;
	 }
	 else
	 {
		if(isSessionPBAP)
		{
			isSessionPBAP = FALSE;
			GVariant *out_configuration = NULL;
			g_signal_connect(session_proxy, "status-changed", G_CALLBACK(status_changed_signal), NULL);
			g_signal_connect(session_proxy, "progress-changed", G_CALLBACK(progress_changed_signal), NULL);
			//org_syncevolution_session_call_attach_sync(session_proxy, NULL, NULL);
			call_set_config(user_data);
			org_syncevolution_session_call_get_config_sync(session_proxy, FALSE, &out_configuration, NULL, &error_getname);
			 if( error_getname )
				 {
					g_warning("Error in GetConfig method %s", error_getname ? error_getname->message : "no error given.");
					g_error_free(error_getname);
					error_getname = NULL;
				 }
			 else
			 {
				 BLUETOOTHSYNC_DNLD_PRINT(" GetConfig response = %s \n", g_variant_print(out_configuration, 1));
			 }

			 GVariant *arg_sources = NULL;
			 GVariantBuilder *builder ;
			 builder = g_variant_builder_new(G_VARIANT_TYPE("a{ss}"));
			 g_variant_builder_add(builder, "{ss}", g_strdup("addressbook"), g_strdup(ONEWAYSYNC));
			 arg_sources = g_variant_builder_end(builder);
			 org_syncevolution_session_call_sync(session_proxy, g_strdup(ONEWAYSYNC), arg_sources, NULL, sync_callback, user_data);			 
			 return;
		}
		else
		{
			isSessionPBAP = TRUE;
			g_signal_connect(session_proxy, "status-changed", G_CALLBACK(status_changed_signal), NULL);
			g_signal_connect(session_proxy, "progress-changed", G_CALLBACK(progress_changed_signal), NULL);
			BLUETOOTHSYNC_DNLD_PRINT("Before calling target conf mac address %s \n", (gchar *)user_data);
			call_set_target_config(user_data);
		}
	 }

}

#if 0
static gboolean g_print_ghash(gpointer key, gpointer value, gpointer user_data)
{

	BLUETOOTHSYNC_DNLD_PRINT("key %s --> value %s and session path = %s \n", (gchar *)key, (gchar *)value, (gchar *)user_data);
	if(g_strcmp0( (gchar *)key, (gchar *)user_data) == 0)
		{
			return TRUE;
		}
	else
		return FALSE;

}
#endif

#if 0
static gboolean g_hash_custom_replace(gpointer key, gpointer value, gpointer user_data)
{

	BLUETOOTHSYNC_DNLD_PRINT("key %s --> value %s and session path = %s \n", (gchar *)key, (gchar *)value, (gchar *)user_data);
	if(g_strcmp0( (gchar *)key, "/org/sync") == 0)
		{
			g_hash_table_replace(ghash_table_session_path, (gchar *)user_data, (gchar *) value);
			return TRUE;
		}
	else
		return FALSE;

}
#endif

//void initialise_org_syncevolution_session_gdbus_proxy(const gchar *session_path)
gboolean initialise_org_syncevolution_session_gdbus_proxy(gpointer data)
{
	const gchar *session_path = (const gchar *)data;
	GDBusConnection *connection;
	GDBusProxyFlags flags = G_DBUS_PROXY_FLAGS_NONE;
	connection = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, NULL);

#if 0
	BLUETOOTHSYNC_DNLD_PRINT("getting mac address for the given session path %s\n", session_path);
	gchar *dup_session_path = g_strdup(session_path);
	g_hash_table_find(ghash_table_session_path, g_print_ghash, dup_session_path);
#endif

	gchar *mac_address = g_hash_table_lookup(ghash_table_session_path, session_path);
	BLUETOOTHSYNC_DNLD_PRINT(" Mac address from GHashtable %s function = %s\n", mac_address, __FUNCTION__);

	org_syncevolution_session_proxy_new(connection,
										flags,
										"org.syncevolution",
										session_path,
										NULL,
										org_syncevolution_session_callback,
										mac_address);
	return FALSE;
}

static void start_session_callback(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s and MAC address is %s\n", __FUNCTION__, (gchar *)user_data);
	GError *error = NULL;
	gchar *out_session = NULL;
	org_syncevolution_server_call_start_session_finish((OrgSyncevolutionServer *)source_object, &out_session, res, &error);
	if (error)
	{
		g_warning("Error in creating syncevolution session  %s", error ? error->message : "no error given.");
		parse_and_send_error_signal(error->message, user_data);
		g_error_free(error);
		error = NULL;
	}
	else
	{
		BLUETOOTHSYNC_DNLD_PRINT(" syncevolution session created successfully and session objectpath = %s \n", out_session);

		g_hash_table_replace(ghash_table_session_path, g_strdup(out_session), (gchar *) user_data); // Key is session path and values is mac address
		gchar *mac_address = g_hash_table_lookup(ghash_table_session_path, out_session);
		BLUETOOTHSYNC_DNLD_PRINT(" Function %s and MAC address is %s\n", __FUNCTION__, mac_address);
		//initialise_org_syncevolution_session_gdbus_proxy(out_session); wait for session state to ready in session_changed signal
	}
}

static void session_changed_signal(OrgSyncevolutionServer *object, const gchar *arg_session, gboolean arg_started)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	if(arg_session == NULL)
	{
		g_warning("session is NULL\n");
		return;
	}

	BLUETOOTHSYNC_DNLD_PRINT(" Session is %s and its status %d \n", arg_session, arg_started);
	if(arg_started)
	{
		BLUETOOTHSYNC_DNLD_PRINT("session state is ready for use \n");
		gchar *session_object_path = g_strdup(arg_session);
		//g_hash_table_find(ghash_table_session_path, g_hash_custom_replace, session_object_path);

		//gchar *mac_address = g_hash_table_lookup(ghash_table_session_path, arg_session);
		//BLUETOOTHSYNC_DNLD_PRINT(" Mac address from GHashtable %s Function = %s \n", mac_address, __FUNCTION__);
		//initialise_org_syncevolution_session_gdbus_proxy(session_object_path);
		g_timeout_add(50, initialise_org_syncevolution_session_gdbus_proxy, session_object_path);
	}
}

static void info_request_changed_signal (OrgSyncevolutionServer *object,
	    								 const gchar *arg_id,
	    								 const gchar *arg_session,
	    								 const gchar *arg_state,
	    								 const gchar *arg_handler,
	    								 const gchar *arg_type,
	    								 GVariant *arg_parameters)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUETOOTHSYNC_DNLD_PRINT(" INFO REQUEST ID = %s, session = %s, state = %s \n",arg_id,  arg_session, arg_state);
	BLUETOOTHSYNC_DNLD_PRINT(" INFO REQUEST handler = %s, type = %s, gpatameter = %s \n",arg_handler,  arg_type, g_variant_print(arg_parameters, 1));
	org_syncevolution_server_call_info_response_sync(server_proxy, arg_id,  arg_state, arg_parameters, NULL, NULL);


}

#if 0
static void get_report_cb(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	GVariant *out_reports = NULL;
	GError *error = NULL;
	org_syncevolution_session_call_get_reports_finish((OrgSyncevolutionSession *)source_object, &out_reports, res, &error);
	if (error)
	{
		g_warning("Error in GetReport  %s", error ? error->message : "no error given.");
		g_error_free(error);
		error = NULL;
	}
	else
	{
		BLUETOOTHSYNC_DNLD_PRINT(" GetReport is %s\n", g_variant_print(out_reports, 1));
	}
}
#endif

static void log_output_signal(OrgSyncevolutionServer *object, const gchar *arg_path, const gchar *arg_level, const gchar *arg_output, const gchar *arg_output2)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	if(g_strcmp0(arg_level, "DEBUG") == 0)
		return;

	gchar *mac_address = g_hash_table_lookup(ghash_table_session_path, arg_path);
	gchar *duplicate_mac_address = g_strdup(mac_address);
	const gchar *mac_address_or_arg_path = mac_address? mac_address:arg_path;

	gchar *original_mac = g_strdup(mac_address);
	gchar *modified_mac = g_malloc0(50);
	if(mac_address != NULL)
	{
		replace_colon_with_underscore(original_mac, modified_mac);
	}

	BLUETOOTHSYNC_DNLD_PRINT(" LogOutput is --> path = %s, level = %s, output = %s, output2 = %s \n", arg_path, arg_level, arg_output, arg_output2);

	if( (g_strrstr(arg_output, "CreateSession failed: GDBus.Error:org.bluez.obex.Error.Failed:") ) && ( (g_strcmp0("ERROR", arg_level)) == 0) )
	{

		if(g_strrstr(arg_output, "error code from SyncEvolution access denied (remote, status 403)") && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("Sync Rejected by user", mac_address_or_arg_path);
			//addressbookcallback_syncevo.fn_error("Sync Rejected by user", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
		else if(g_strrstr(arg_output, " CreateSession failed: GDBus.Error:org.bluez.obex.Error.Failed: Software caused connection abort (103)") && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("Connection abort", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
		else if(g_strrstr(arg_output, "error code from SyncEvolution fatal error (local, status 10500): sync failed") && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("User response timed out", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
		else if(g_strrstr(arg_output, "error code from SyncEvolution access denied (remote, status 403): sync failed") && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("Sync Rejected by user", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
	
		else if(g_strrstr(arg_output, "No adapter found")&& (g_strrstr(arg_output2, modified_mac ) ) && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("BT is OFF", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
		else if( (g_strrstr(arg_output, "Timed out waiting for response") )  && (g_strrstr(arg_output2, modified_mac ) ) && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("User response timed out", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
		else if(g_strrstr(arg_output, "Unable to find service record") && (g_strrstr(arg_output2, modified_mac ) ) && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("Phone Not reachable", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}

		else if(g_strrstr(arg_output, "error code from SyncEvolution access denied (remote, status 403): sync failed"))
		{
			error_cb("Sync Rejected by user", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
		else if(g_strrstr(arg_output, "Transport got disconnected")&& (g_strrstr(arg_output2, modified_mac ) ) && ( (g_strcmp0("ERROR", arg_level)) == 0) )
		{
			error_cb("Sync Rejected by user", mac_address_or_arg_path);
			//g_hash_table_free((gpointer)duplicate_mac_address);
		}
		else
		{
			if(mac_address != NULL)
			{
				error_cb("Sync Failed", mac_address_or_arg_path);
			}
		}
		g_hash_table_free((gpointer)duplicate_mac_address);
	}
	else if( (g_strrstr(arg_output, "no changes") ) && (g_strrstr(arg_output2, modified_mac ) ) )
	{
		gchar *mac_null_check = g_hash_table_lookup(ghash_table_session_path, arg_path);
		if(mac_null_check != NULL)
		{
			BluezMng *self = bluez_mgr_get_object();
			CorbridgeAddressBook *obj = get_corbridge_address_book_object(self);
			if(obj != NULL)
			corbridge_address_book_emit_complete(obj, mac_address_or_arg_path);
		}
		g_hash_table_free((gpointer)duplicate_mac_address);
	}
	else if(g_strrstr(arg_output, "/addressbook: sent") && g_strrstr(arg_output2, modified_mac ))
	{
		gchar *s3 = NULL, *dup_arg_output = NULL;
		dup_arg_output = g_strdup(arg_output + 23);
		strtok(dup_arg_output , "/");
		s3 = strtok(NULL, "/");
		SyncevoInfo *syncevo_info = g_new0(SyncevoInfo, 1);
		syncevo_info->maxcount = atoi(s3);
		syncevo_info->count = 1;
		syncevo_info->message = g_strdup("");
		g_hash_table_replace(ghash_table_info, g_strdup(arg_path), syncevo_info);
		BLUETOOTHSYNC_DNLD_PRINT("MAX COUNT %d\n", syncevo_info->maxcount);
		g_free(dup_arg_output);
	}
	else if(g_strrstr(arg_output, "@default/addressbook: added"))
	{
		gpointer value;
		SyncevoInfo *update_hashtable, *syncevo_info1;
		value = g_hash_table_lookup(ghash_table_info, arg_path);
		update_hashtable = (SyncevoInfo *)value;
		BLUETOOTHSYNC_DNLD_PRINT("INC COUNT %d\n",update_hashtable->count);

		if(update_hashtable->count == update_hashtable->maxcount)
		{
			gchar *msg = g_strdup_printf("Sync completed with %s", arg_output + 21);

			BluezMng *self = bluez_mgr_get_object();
			CorbridgeAddressBook *obj = get_corbridge_address_book_object(self);
			if(obj != NULL)
				corbridge_address_book_emit_info(obj,msg, mac_address_or_arg_path);

			g_hash_table_remove(ghash_table_info, arg_path);
		}
		else
		{
			syncevo_info1 = g_new0(SyncevoInfo, 1);
			syncevo_info1->maxcount = update_hashtable->maxcount;
			syncevo_info1->count = update_hashtable->count + 1;
			syncevo_info1->message = g_strdup(arg_output + 21);
			g_hash_table_insert(ghash_table_info, g_strdup(arg_path), syncevo_info1);
		}
	}
	else if(g_strrstr(arg_output,"PBAP content pulled:"))
	{
		// send the no of entries pulled from phone
	}
	else if(g_strrstr(arg_output,"@pbap/addressbook: normal sync done successfully"))
	{
		//org_syncevolution_session_call_get_reports(session_proxy, 0, 10, NULL, get_report_cb, NULL);
	}
	else if( ( (g_strcmp0("The connection is closed", arg_output) == 0) ) && ( (g_strcmp0("ERROR", arg_level) == 0) ) )
	{
		if(mac_address != NULL)
		{
			error_cb("Sync Failed because of backend crash", mac_address_or_arg_path);
		}
		g_hash_table_free((gpointer)duplicate_mac_address);
	}
	else if( (g_strrstr(arg_output, "error code from SyncEvolution fatal error (local, status 10500): sync failed") ) && ( (g_strcmp0("ERROR", arg_level) == 0) ) )
	{
		if(mac_address != NULL)
		{
			error_cb("Sync Failed", mac_address_or_arg_path);
		}
		g_hash_table_free((gpointer)duplicate_mac_address);
	}
	else if( (g_strrstr(arg_output, "aborted on behalf of user (local, status 20017)") ) && ( (g_strcmp0("ERROR", arg_level) == 0) ) )
	{
		if(mac_address != NULL)
		{
			error_cb("Sync Failed", mac_address_or_arg_path);
		}
		g_hash_table_free((gpointer)duplicate_mac_address);
	}
	g_free(duplicate_mac_address);
	duplicate_mac_address = NULL;
	g_free(original_mac);
	original_mac = NULL;
	g_free(modified_mac);
	modified_mac = NULL;
	BLUETOOTHSYNC_DNLD_PRINT("\n\n\n\n");
}

static void presence_changed_signal( OrgSyncevolutionServer *object, const gchar *arg_server, const gchar *arg_status, const gchar *arg_transport)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);
	BLUETOOTHSYNC_DNLD_PRINT("server = %s, status = %s, transport = %s \n", arg_server, arg_status, arg_transport);
}

static void org_syncevolution_callback(GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n",__FUNCTION__);

	GError *error = NULL;
	server_proxy = org_syncevolution_server_proxy_new_finish(res, &error);
	 if( (server_proxy == NULL) || error )
	 {
		g_warning("Error initializing the org.syncevolution proxy %s", error ? error->message : "no error given.");
		g_error_free(error);
		error = NULL;
	 }
	 else
	 {
		BLUETOOTHSYNC_DNLD_PRINT(" org.syncevolution proxy created successfully \n");
		g_signal_connect(server_proxy, "session-changed", G_CALLBACK(session_changed_signal), NULL);
		g_signal_connect(server_proxy, "info-request", G_CALLBACK(info_request_changed_signal), NULL);
		g_signal_connect(server_proxy, "presence", G_CALLBACK(presence_changed_signal), NULL);
		g_signal_connect(server_proxy, "log-output", G_CALLBACK(log_output_signal), NULL);
	 }
}


/**
 * syncevolution dbus API.
 * This API creates syncevolution server proxy and registers for session-changed signal, log-output, presence and info-request
 */
void initialise_org_syncevolution_gdbus_proxy()
{
	  const char *env_string;
	  env_string = g_getenv("BLUETOOTHSYNC_SRV_DEBUG");
	  if (env_string != NULL)
	    {
		  bluetooth_sync_service_debug_flags = g_parse_debug_string(env_string, bluetooth_sync_debug_keys, G_N_ELEMENTS(bluetooth_sync_debug_keys));
	    }
	  //TODO:: uncomment this one
	 //log_register("addressbook.log");

	BLUETOOTHSYNC_DNLD_PRINT(" Function %s\n", __FUNCTION__);
	GDBusConnection *connection;
	//gpointer user_data = NULL;
	GDBusProxyFlags flags = G_DBUS_PROXY_FLAGS_NONE;
	connection = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, NULL);
	g_hash_for_session_path_init();

	org_syncevolution_server_proxy_new(connection,
									flags,
		    						"org.syncevolution",
		    						"/org/syncevolution/Server",
		    						NULL,
		    						org_syncevolution_callback,
		    						NULL);
}

void cancel_inprogress_request_syncevo(const gchar *arg_mac_address)
{
	GError *error_detach = NULL;
	gchar *value = g_strdup(arg_mac_address);
	gpointer value_data = (gpointer)value;
    gpointer *key_value = NULL;
    key_value = g_hash_table_find(ghash_table_session_path, g_print_mac_existance_ghash, value_data);
    if(key_value != NULL)
    {
    	error_cb("already canceled", arg_mac_address);
    }
    else
    {
		//TODO: call detach or abort method to cancel the ongoing sync or get the session path from hash table and call abort for each session path

		org_syncevolution_session_call_detach_sync(session_proxy, NULL, &error_detach);
		if (error_detach)
		{
			g_warning("Error in Detachhhh method %s", error_detach ? error_detach->message : "no error given.");
			if( g_strrstr(error_detach->message, "cannot detach from resource that client is not attached to"))
			{
				parse_and_send_error_signal("Not in progress or already canceled", (gpointer)arg_mac_address);
			}
			else
			{
				parse_and_send_error_signal(error_detach->message, (gpointer)arg_mac_address);
			}
			g_error_free(error_detach);
			error_detach = NULL;
		}
		else
		{
			BLUETOOTHSYNC_DNLD_PRINT("Cancel success \n");
		}

	}

    g_free(value);
    value = NULL;
}

/**
 * Description: Used by HMI to know ongoing Sync status.
 * mac_address: Check the sync status for given BT device
 * Return value: returns TRUE if sync in progress for the given mac_address else returns FALSE
 */
gboolean get_sync_status_syncevo(const gchar *mac_address)
{
	if(mac_address == NULL)
	{
		return FALSE;
	}
	else
	{
		BLUETOOTHSYNC_DNLD_PRINT(" Function %s and MAC address is %s\n", __FUNCTION__, mac_address);
		gchar *value = g_strdup(mac_address);
		gpointer value_data = (gpointer)value;
	    gpointer *key_value = NULL;
	    key_value = g_hash_table_find(ghash_table_session_path, g_print_mac_existance_ghash, value_data);
	    if(key_value != NULL)
	    {
	    	g_free(value);
	    	value = NULL;
	        return TRUE;
	    }
	    else
	    {
	    	return FALSE;
	    }
	}
}

/**
 * mac_address: Phone mac address
 * description: fetches the contacts from given mac address device. And adds contacts automatically to EDS.
 */
void get_contacts_from_syncevolution(const gchar *mac_address)
{
	if(mac_address == NULL)
	{
		error_cb("Mac address is not valid", "FF:FF:FF:FF:FF:FF");
		return;
	}
	BLUETOOTHSYNC_DNLD_PRINT(" Function %s and MAC address is %s\n", __FUNCTION__, mac_address);
	gchar *value = g_strdup(mac_address);
	gpointer value_data = (gpointer)value;
    gpointer *key_value = NULL;
    key_value = g_hash_table_find(ghash_table_session_path, g_print_mac_existance_ghash, value_data);
    if(key_value != NULL)
    {   // send the error signal saying that sync in progress
    	error_cb("Sync in progress !", mac_address);
    	g_free(value);
    	value = NULL;
        return;
    }
    else
    {
    BLUETOOTHSYNC_DNLD_PRINT(" Function %s and New MAC address is %s\n", __FUNCTION__, mac_address);
    g_free(value);
    value = NULL;
    }

	gchar *dup_mac_address = g_strdup(mac_address);
	gpointer user_data = (gpointer)dup_mac_address;
	gchar *configname = g_strdup_printf("target-config@%s", mac_address);
	//org_syncevolution_server_call_start_session(server_proxy, target-config@pbap, NULL, start_session_callback, user_data);
	org_syncevolution_server_call_start_session(server_proxy, configname, NULL, start_session_callback, user_data);
	g_free(configname);
	configname = NULL;
}
