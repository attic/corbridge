/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CORBRIDGE_H_
#define CORBRIDGE_H_

#include "org.apertis.Corbridge.AddressBook.h"
#include "org.apertis.Corbridge.Control.h"

#endif /* CORBRIDGE_H_ */
