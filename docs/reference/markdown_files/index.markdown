# Corbridge

Corbridge is daemon for managing bluetooth functionalities like pairing,
unpairing, A2DP connect/disconnect. And also contact Sync via bluetooth
also implemented in this service.

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
